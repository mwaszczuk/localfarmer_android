package pl.solventive.localfarmer.util.components

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.text.InputFilter
import android.text.InputType
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.component_lf_edit_text.view.*
import kotlinx.android.synthetic.main.component_lf_pin.view.*
import pl.solventive.localfarmer.R

/**
 * This is a custom LF Pin class that serves the purposes of displaying the PIN dots and handling the PIN input.
 *
 * @author  Marek Waszczuk
 * @version 1.0
 * @since   23.03.2020
 */

class LFPin @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs) {

    var length = 0
    var currentPin: String = ""
    set(value) {
        if (value.length > 4) return
        field = value
        updateView()
    }

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.component_lf_pin, this, true)
    }

    private fun updateView() {
        if (length < currentPin.length) {
            when (currentPin.length) {
                1 -> { greenCircle1.animate().alpha(1f).duration = 300 }
                2 -> { greenCircle2.animate().alpha(1f).duration = 300 }
                3 -> { greenCircle3.animate().alpha(1f).duration = 300 }
                4 -> { greenCircle4.animate().alpha(1f).duration = 300 }
            }
            length++
        } else {
            when (currentPin.length) {
                0 -> { greenCircle1.animate().alpha(0f).duration = 300 }
                1 -> { greenCircle2.animate().alpha(0f).duration = 300 }
                2 -> { greenCircle3.animate().alpha(0f).duration = 300 }
                3 -> { greenCircle4.animate().alpha(0f).duration = 300 }
            }
            length--
        }
    }

    fun addDigit(digit: String) {
        currentPin = currentPin.plus(digit)
    }

    fun addDigit(digit: Int) {
        currentPin = currentPin.plus(digit.toString())
    }

    fun dropLast() {
        currentPin = currentPin.dropLast(1)
    }

    fun clear() {
        greenCircle1.animate().alpha(0f).duration = 300
        greenCircle2.animate().alpha(0f).duration = 300
        greenCircle3.animate().alpha(0f).duration = 300
        greenCircle4.animate().alpha(0f).duration = 300
        length = 0
        currentPin = ""
    }

    fun showError() {
        clear()
        val anim = AnimationUtils.loadAnimation(context, R.anim.shake)
        this.startAnimation(anim)
    }

}