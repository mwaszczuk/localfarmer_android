package pl.solventive.localfarmer.util.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.solventive.localfarmer.main.MainActivity

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [
        FragmentBuildersModule::class,
        DialogModule::class
    ])
    abstract fun contributeMainActivity(): MainActivity
}