package pl.solventive.localfarmer.util.components

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.constraintlayout.widget.ConstraintLayout

class LFConstraintLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : ConstraintLayout(context, attrs) {

    var catchTouchEvents = false

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (catchTouchEvents && ev?.action == MotionEvent.ACTION_DOWN) return true
        return super.onInterceptTouchEvent(ev)
    }

}