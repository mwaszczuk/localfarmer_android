package pl.solventive.localfarmer.util.components

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.component_lf_button.view.*
import pl.solventive.localfarmer.R

class LFButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs) {

    private val alphaAnimation = AlphaAnimation(1.0f, 0.0f)
    private var animText = ""

    var isActive: Boolean = true
        set(value) {
            if (field == value) return
            if (value) buttonLayout?.background?.setTint(activeColor)
            else buttonLayout?.background?.setTint(inactiveColor)
            clickableArea.isClickable = value
            field = value
        }

    var inactiveColor: Int = ContextCompat.getColor(context, R.color.grey_light)
    var activeColor: Int = ContextCompat.getColor(context, R.color.lfGreen)

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.component_lf_button, this, true)
        alphaAnimation.duration = 200
        alphaAnimation.repeatCount = 1
        alphaAnimation.repeatMode = Animation.REVERSE

        alphaAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationEnd(animation: Animation?) { }
            override fun onAnimationStart(animation: Animation?) { }
            override fun onAnimationRepeat(animation: Animation?) { buttonText.text = animText }
        })

        val attrArray =
            context.obtainStyledAttributes(attrs, R.styleable.LFButton, defStyle, defStyleRes)
        loadAttributes(attrArray)
    }

    private fun loadAttributes(attrs: TypedArray) {
        val text = attrs.getText(R.styleable.LFButton_text)
        val textColor = attrs.getColor(R.styleable.LFButton_textColor, -1)
        val textSize = attrs.getInt(R.styleable.LFButton_buttonTextSize, -1)
        val activeColor = attrs.getColor(R.styleable.LFButton_activeColor, -1)
        val inactiveColor = attrs.getColor(R.styleable.LFButton_inactiveColor, -1)
        val isActive = attrs.getBoolean(R.styleable.LFButton_isActive, true)
        attrs.recycle()

        buttonText.text = text
        if (textColor != -1) buttonText.setTextColor(textColor)
        if (textSize != -1) buttonText.textSize = textSize.toFloat()
        if (activeColor != -1) { this.activeColor = activeColor }
        if (inactiveColor != -1) this.inactiveColor = inactiveColor
        if (!isActive) this.isActive = false

        clickableArea.setOnClickListener { hideKeyboard() }
    }

    override fun setOnClickListener(l: OnClickListener?) {
        clickableArea.setOnClickListener {
            hideKeyboard()
            l?.onClick(clickableArea)
        }
    }

    /**
     * Changes to a given text with alpha out alpha in animation
     */
    fun changeText(text: String) {
        animText = text
        buttonText.startAnimation(alphaAnimation)
    }

    fun hideKeyboard() {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(windowToken, 0)
    }

}
