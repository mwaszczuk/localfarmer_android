package pl.solventive.localfarmer.util.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.component_small_image.view.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.util.extension.makeInvisible
import pl.solventive.localfarmer.util.extension.makeVisible

class LFSmallImage @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.component_small_image, this, true)

        this.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_lighter))

    }

    fun getImage() : ImageView = imgSource

    fun showImage() {
        imgPhoto.makeInvisible()
        imgSource.animate().alpha(1f).duration = 300
    }

    fun hideImage() {
        imgPhoto.makeVisible()
        imgSource.animate().alpha(0f).duration = 300
    }
}
