package pl.solventive.localfarmer.util.components

import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.res.TypedArray
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.android.synthetic.main.component_lf_edit_text.view.*
import kotlinx.coroutines.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.util.extension.makeGone
import pl.solventive.localfarmer.util.extension.makeVisible

/**
 * This is a custom EditText wrapper class that combines EditText with other layout items.
 * It provides a nicely styled EditText View that can be easily used in the whole app using attributes in XML.
 *
 * @author  Marek Waszczuk
 * @version 1.0
 * @since   21.03.2020
 */

class LFEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs), CoroutineScope by MainScope() {

    private val alphaAnimation = AlphaAnimation(1.0f, 0.0f)
    var animDrawable: Int? = null

    private var charactersCountVisible = false
    private var isPhoneNumber = false
    private var maxLength: Int = -1
    private var hideKeyboardAutomatically = false
    private var acceptIfNotEmpty = false
    private var debounceText = false
    private var textJob: Job? = null

    var rawLength = 0
    var length: Int = 0
        set(value) {
            if (charactersCountVisible) {
                if (maxLength > -1) {
                    txtCharactersCount.text = resources.getString(R.string.component_LF_edit_text_txt_length_capped, value.toString(), maxLength.toString())
                    if (value == maxLength) {
                        if (hideKeyboardAutomatically) hideKeyboard()
                        showIndicator(R.drawable.ic_success_circle)
                    } else if (!acceptIfNotEmpty) showIndicator(R.drawable.ic_empty_circle)
                } else txtCharactersCount.text = resources.getString(
                    R.string.component_LF_edit_text_txt_length,
                    value.toString()
                )
            }
            field = value
        }
    private val currentText = MutableLiveData("")
    private var isErrorShown = false

    private val textWatcher = object: TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            if (debounceText) {
                if (textJob != null && textJob!!.isActive) textJob?.cancel()
                textJob = launch {
                    delay(500)
                    onTextChanged(p0?.toString() ?: "")
                }
            } else {
                onTextChanged(p0?.toString() ?: "")
            }
        }
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, count: Int) {
            if (acceptIfNotEmpty && !p0.isNullOrEmpty()) showAccepted()
            else showIndicator(R.drawable.ic_empty_circle)
            if (isErrorShown) hideError()
        }
    }

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.component_lf_edit_text, this, true)

        setupAlphaAnimation()

        val attrArray =
            context.obtainStyledAttributes(attrs, R.styleable.LFEditText, defStyle, defStyleRes)
        loadAttributes(attrArray)
        innerEditText.addTextChangedListener(textWatcher)
    }

    private fun setupAlphaAnimation() {
        alphaAnimation.duration = 200
        alphaAnimation.repeatCount = 1
        alphaAnimation.repeatMode = Animation.REVERSE

        alphaAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationEnd(animation: Animation?) { }
            override fun onAnimationStart(animation: Animation?) { }
            override fun onAnimationRepeat(animation: Animation?) {
                if (animDrawable != null)
                    imgStatus.setImageDrawable(ContextCompat.getDrawable(context, animDrawable!!))
            }
        })
    }

    /**
     * loads XML attributes.
     */
    private fun loadAttributes(attrs: TypedArray) {
        val headerText = attrs.getText(R.styleable.LFEditText_headerText)
        val text = attrs.getText(R.styleable.LFEditText_text)
        val hint = attrs.getText(R.styleable.LFEditText_hint)
        val charactersCountVisible = attrs.getBoolean(R.styleable.LFEditText_charactersCountVisible, false)
        val phoneNumber = attrs.getBoolean(R.styleable.LFEditText_isPhoneNumber, false)
        acceptIfNotEmpty = attrs.getBoolean(R.styleable.LFEditText_acceptIfNotEmpty, false)
        val hideKeyboardAutomatically = attrs.getBoolean(R.styleable.LFEditText_hideKeyboardAutomatically, false)
        debounceText = attrs.getBoolean(R.styleable.LFEditText_debounceText, false)
        val maxLength = attrs.getInt(R.styleable.LFEditText_maxLength, -1)
        val numeric = attrs.getBoolean(R.styleable.LFEditText_numeric, false)
        val description = attrs.getText(R.styleable.LFEditText_description)
        if (attrs.getBoolean(R.styleable.LFEditText_indicatorVisibility, false)) linearStatus.makeVisible()
        if (!attrs.getBoolean(R.styleable.LFEditText_savingEnabled, true)) innerEditText.isSaveEnabled = false
        attrs.recycle()

        if (description != null) txtCharactersCount.text = description
        if (text != null) innerEditText.setText(text)
        if (numeric) {
            innerEditText.inputType = InputType.TYPE_CLASS_NUMBER
            innerEditText.keyListener = DigitsKeyListener.getInstance("1234567890")
        }
        if (headerText != null) txtHeader.text = headerText
        if (hint != null) innerEditText.hint = hint
        if (maxLength != -1) {
            this.maxLength = maxLength
            innerEditText.filters = arrayOf(InputFilter.LengthFilter(maxLength))
        }
        if (charactersCountVisible) {
            this.charactersCountVisible = charactersCountVisible
            when (maxLength) {
                -1 -> txtCharactersCount.text =
                    resources.getString(R.string.component_LF_edit_text_txt_length, "0")
                else -> txtCharactersCount.text = resources.getString(
                    R.string.component_LF_edit_text_txt_length_capped,
                    "0",
                    maxLength.toString()
                )
            }
        }

        if (phoneNumber) {
            this.isPhoneNumber = true
            innerEditText.inputType = InputType.TYPE_CLASS_NUMBER
            innerEditText.keyListener = DigitsKeyListener.getInstance("1234567890")
            this.maxLength = 9
            innerEditText.filters = arrayOf(InputFilter.LengthFilter(11))
        }
        if (hideKeyboardAutomatically) this.hideKeyboardAutomatically = hideKeyboardAutomatically
    }

    /**
     * Parses changed text and changes it accordingly to EditText options:
     *
     * - isPhoneNumber
     * - maxLength
     * - noBlankSpaces
     *
     * @param text - text received from EditText change.
     */
    private fun onTextChanged(text: String) {
        val editedText = innerEditText.text.toString()
        if (isPhoneNumber) {
            parseTextPhoneNumber(text, editedText)
        } else {
            currentText.value = text
            length = editedText.length
        }
    }

    /**
     * Changes the text to look like Polish phone number (XXX XXX XXX).
     * It ignores blank spaces when counting the text length and puts a blank space every 3 digits.
     *
     * @param text - text to be parsed to meet phone number standards. This should come from EditText TextWatcher.
     * @param editedText - a current text stored in EditText. This should come from EditText.text
     */
    private fun parseTextPhoneNumber(text: String, editedText: String) {
        currentText.value = text.replace(" ", "")
        if (rawLength <= text.length && (text.length == 3 || text.length == 7)) {
            innerEditText.setText(text.plus(" "))
            innerEditText.setSelection(innerEditText.text.length)
        } else if (rawLength >= text.length && (text.length == 3 || text.length == 7)) {
            innerEditText.setText(text.dropLast(1))
            innerEditText.setSelection(innerEditText.text.length)
        }
        length = editedText.replace(" ", "").length
        rawLength = innerEditText.text.length
    }

    /**
     * Shows an icon next to the title of the EditText. No action is taken when the same icon is already displayed.
     *
     * @param drawableRes - an drawable resource id to be shown
     */
    fun showIndicator(drawableRes: Int) {
        if (animDrawable != drawableRes) {
            animDrawable = drawableRes
            imgStatus.startAnimation(alphaAnimation)
        }
    }

    /**
     * Hides the soft input keyboard
     */
    fun hideKeyboard() {
        (context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(windowToken, 0)
    }

    /**
     * A method to indicate an input error with this EditText.
     * UI Error display consists of:
     * - display the error message at the bottom of the component
     * - change certain items color to red and vibrate them
     *
     * @param message - a message to be displayed when an error occurs
     */
    fun showError(message: String) {
        isErrorShown = true
        txtError.makeVisible()
        showIndicator(R.drawable.ic_error_circle)
        txtError.text = message
        innerEditText.setTextColor(ContextCompat.getColor(context, R.color.red))
        txtError.alpha = 1f
        val anim = AnimationUtils.loadAnimation(context, R.anim.shake)
        this.startAnimation(anim)
    }

    /**
     * shows green icon in the component.
     */
    fun showAccepted() {
        showIndicator(R.drawable.ic_success_circle)
    }

    /**
     * Hides error
     */
    fun hideError() {
        txtError.makeGone()
        isErrorShown = false
        innerEditText.setTextColor(ContextCompat.getColor(context, R.color.grey_medium))
        txtError.animate().alpha(0f).duration = 200
    }

    /**
     * Returns a LiveData with current text.
     */
    fun observeTextChanges() = currentText as LiveData<String>

    fun isNotEmpty() = innerEditText.length() > 0

    fun getText() = innerEditText.text.toString()

    fun setText(text: String, observe: Boolean = false) {
        if (!observe) innerEditText.removeTextChangedListener(textWatcher)
        innerEditText.setText(text)
        if (!observe) innerEditText.addTextChangedListener(textWatcher)
    }
}
