package pl.solventive.localfarmer.util.components.converter

import com.google.gson.*
import org.joda.time.DateTime
import java.lang.reflect.Type

class DateTimeSerializer : JsonSerializer<DateTime> {

    override fun serialize(src: DateTime?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return JsonPrimitive(src?.toString("yyyy-MM-dd'T'HH:mm:ss"))
    }

}