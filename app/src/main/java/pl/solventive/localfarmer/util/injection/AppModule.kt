package pl.solventive.localfarmer.util.injection

import android.app.Application
import androidx.preference.PreferenceManager
import androidx.room.Room
import com.google.gson.GsonBuilder
import com.ktopencage.OpenCageGeoCoder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.joda.time.DateTime
import pl.solventive.localfarmer.BuildConfig
import pl.solventive.localfarmer.LocalFarmerApplication
import pl.solventive.localfarmer.data.OkHttpInterceptor
import pl.solventive.localfarmer.data.database.Database
import pl.solventive.localfarmer.data.network.api.*
import pl.solventive.localfarmer.util.components.converter.DateTimeDeserializer
import pl.solventive.localfarmer.util.components.converter.DateTimeSerializer
import pl.solventive.localfarmer.util.components.converter.GsonDateDeserializer
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.*
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        val interceptor: Interceptor = if (BuildConfig.DEBUG) {
            Interceptor {
                val response = it.proceed(it.request())
                Timber.tag("API LOG").d(response.code.toString())
                if (response.code != 200 && response.code != 201 && response.code != 204) {
                    Timber.tag("API ERROR").d(it.request().url.toString() + "\r\n" + response.peekBody(500).string())
                }
                response
            }
        } else OkHttpInterceptor()

        return Retrofit.Builder()
            .baseUrl(LocalFarmerApplication.API_ADRESS)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                .registerTypeAdapter(Date::class.java, GsonDateDeserializer())
                .registerTypeAdapter(DateTime::class.java, DateTimeDeserializer())
                .registerTypeAdapter(DateTime::class.java, DateTimeSerializer())
                .create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(OkHttpClient.Builder().addInterceptor(interceptor).build())
            .build()
    }

    @Provides
    fun provideSharedPrefs(app: Application)
            = PreferenceManager.getDefaultSharedPreferences(app)

    @Singleton
    @Provides
    fun provideOpenCageGeoCoder()
            = OpenCageGeoCoder(BuildConfig.GEOCODER_KEY)

    @Singleton
    @Provides
    fun provideLoginApi(retrofit: Retrofit) = retrofit.create(LoginApi::class.java)

    @Singleton
    @Provides
    fun provideUsersApi(retrofit: Retrofit) = retrofit.create(UsersApi::class.java)

    @Singleton
    @Provides
    fun provideSmsApi(retrofit: Retrofit) = retrofit.create(SmsApi::class.java)

    @Singleton
    @Provides
    fun provideProductsApi(retrofit: Retrofit) = retrofit.create(ProductsApi::class.java)

    @Singleton
    @Provides
    fun providePostingsApi(retrofit: Retrofit) = retrofit.create(PostingsApi::class.java)

    @Singleton
    @Provides
    fun provideFilesApi(retrofit: Retrofit) = retrofit.create(FilesApi::class.java)

    @Singleton
    @Provides
    fun provideLocationsApi(retrofit: Retrofit) = retrofit.create(LocationsApi::class.java)

    @Singleton
    @Provides
    fun provideDb(app: Application)
            = Room.databaseBuilder(app, Database::class.java, "LocalFarmer")
        .fallbackToDestructiveMigration().build()

    @Provides
    fun provideProductUnitDao(db: Database) = db.productUnitDao()

    @Provides
    fun provideProductCategoryDao(db: Database) = db.productCategoryDao()
}