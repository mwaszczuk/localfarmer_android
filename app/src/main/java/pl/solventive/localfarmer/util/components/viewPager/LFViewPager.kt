package pl.solventive.localfarmer.util.components.viewPager

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.animation.Interpolator
import androidx.viewpager.widget.ViewPager


class LFViewPager @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : ViewPager(context, attrs) {

    enum class SwipeDirection {
        Left, Right, All, None
    }

    private var initialXValue = 0f
    var direction = SwipeDirection.All
    private var customScroller: LFScroller? = null

    init {
        val viewPager = ViewPager::class.java
        val scroller = viewPager.getDeclaredField("mScroller")
        scroller.isAccessible = true
        val interpolator = viewPager.getDeclaredField("sInterpolator")
        interpolator.isAccessible = true

        customScroller = LFScroller(getContext(), interpolator.get(null) as Interpolator)
        scroller.set(this, customScroller)
    }
    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        if (isSwipeAllowed(ev)) {
            return super.onTouchEvent(ev)
        }
        return false
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (isSwipeAllowed(ev)) {
            return super.onInterceptTouchEvent(ev)
        }
        return false
    }

    private fun isSwipeAllowed(event: MotionEvent?): Boolean {
        if (direction == SwipeDirection.None) return false
        if (event != null) {
            if (event.action == MotionEvent.ACTION_DOWN) {
                initialXValue = event.x
                return true
            }
            if (event.action == MotionEvent.ACTION_MOVE) {
                val diffX = event.x - initialXValue
                if (diffX > 0 && direction == SwipeDirection.Right) {
                    return false
                } else if (diffX < 0 && direction == SwipeDirection.Left) {
                    return false
                }
            }
        }
        return true
    }

    fun setScrollDuration(duration: Int) {
        if (customScroller != null) {
            customScroller!!.setScrollDuration(duration)
        }
    }
}