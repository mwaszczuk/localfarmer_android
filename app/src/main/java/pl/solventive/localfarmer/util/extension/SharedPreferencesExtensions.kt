package pl.solventive.localfarmer.util.extension

import android.content.SharedPreferences
import com.google.gson.Gson

private const val USER_PHONE = "currentUserLogin"
private const val USER_PASSWORD = "currentUserPassword"
private const val USER_NAME = "currentUserName"
private const val OAUTH_TOKEN = "currentUserToken"
private const val REFRESH_TOKEN = "currentUserJwt"
private const val USER_ID = "currentUserId"
private const val USER_FIREBASE_TOKEN = "firebaseToken"
private const val APP_IN_FOREGROUND = "appInForeground"
private const val DEVICE_ID = "deviceId"
private const val MAP_PREFERENCES = "mapPreferences"

fun SharedPreferences.getUserPhone(): String {
    return getString(USER_PHONE, "") ?: ""
}

fun SharedPreferences.saveUserPhone(phone: String) {
    edit().putString(USER_PHONE, phone).apply()
}

fun SharedPreferences.getUserPassword(): String {
    return getString(USER_PASSWORD, "") ?: ""
}

fun SharedPreferences.saveUserPassword(password: String) {
    edit().putString(USER_PASSWORD, password).apply()
}

fun SharedPreferences.getUserName(): String {
    return getString(USER_NAME, "") ?: ""
}

fun SharedPreferences.saveUserName(name: String) {
    edit().putString(USER_NAME, name).apply()
}

fun SharedPreferences.getOAuthToken(): String {
    return getString(OAUTH_TOKEN, "") ?: ""
}

fun SharedPreferences.saveOAuthToken(token: String) {
    edit().putString(OAUTH_TOKEN, token).apply()
}

fun SharedPreferences.getRefreshToken(): String {
    return getString(REFRESH_TOKEN, "") ?: ""
}

fun SharedPreferences.saveRefreshToken(token: String) {
    edit().putString(REFRESH_TOKEN, token).apply()
}

//fun SharedPreferences.getUserFirebaseToken(): String {
//    return getString(USER_FIREBASE_TOKEN, "")
//}
//
//fun SharedPreferences.saveUserFirebaseToken(token: String) {
//    edit().putString(USER_FIREBASE_TOKEN, token).apply()
//}

fun SharedPreferences.getUserId(): Int {
    return getInt(USER_ID, -1)
}

fun SharedPreferences.saveUserId(id: Int) {
    edit().putInt(USER_ID, id).apply()
}

fun SharedPreferences.clearUserData() {
    edit().remove(USER_PHONE)
        .remove(USER_PASSWORD)
        .remove(REFRESH_TOKEN)
        .remove(OAUTH_TOKEN)
        .remove(USER_NAME)
        .remove(USER_ID).apply()
}

fun SharedPreferences.getDeviceId(): String {
    return getString(DEVICE_ID, "") ?: ""
}

fun SharedPreferences.saveDeviceId(id: String) {
    edit().putString(DEVICE_ID, id).apply()
}

//fun SharedPreferences.getMapPreferences(): MapPreferences {
//    return when (val json = getString(MAP_PREFERENCES, null)) {
//        null -> MapPreferences.getDefault()
//        else -> Gson().fromJson(json, MapPreferences::class.java)
//    }
//}
//
//fun SharedPreferences.saveMapPreferences(prefs: MapPreferences) {
//    val json = Gson().toJson(prefs)
//    edit().putString(MAP_PREFERENCES, json).apply()
//}