package pl.solventive.localfarmer.util.components.viewPager

import android.content.Context
import android.view.animation.Interpolator
import android.widget.Scroller

class LFScroller : Scroller {

    constructor(context: Context) : super(context) { }
    constructor(context: Context, interpolator: Interpolator) : super(context, interpolator) { }
    constructor(context: Context, interpolator: Interpolator, flywheel: Boolean) : super(context, interpolator, flywheel) { }

    private var scrollDuration = 200

    fun setScrollDuration(duration: Int) {
        scrollDuration = duration
    }

    override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) {
        super.startScroll(startX, startY, dx, dy, scrollDuration)
    }

    override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int) {
        super.startScroll(startX, startY, dx, dy, scrollDuration)
    }
}