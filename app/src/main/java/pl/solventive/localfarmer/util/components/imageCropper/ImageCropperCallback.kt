package pl.solventive.localfarmer.util.components.imageCropper

import android.graphics.Bitmap

interface ImageCropperCallback {

    fun onImageCropped(image: Bitmap, imageType: Int)
}