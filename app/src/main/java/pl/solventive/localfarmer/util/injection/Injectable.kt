package pl.solventive.localfarmer.util.injection

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable