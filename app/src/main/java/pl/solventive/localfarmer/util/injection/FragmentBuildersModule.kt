package pl.solventive.localfarmer.util.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.solventive.localfarmer.main.addPosting.AddPostingFragment
import pl.solventive.localfarmer.main.addPosting.categoryPicker.CategoryPickerFragment
import pl.solventive.localfarmer.main.addPosting.photoPicker.tabs.images.PhotoPickerImagesFragment
import pl.solventive.localfarmer.main.addPosting.price.PriceFragment
import pl.solventive.localfarmer.main.addPosting.quantity.QuantityFragment
import pl.solventive.localfarmer.main.favourites.FavouritesFragment
import pl.solventive.localfarmer.main.help.HelpFragment
import pl.solventive.localfarmer.main.info.InfoFragment
import pl.solventive.localfarmer.main.login.LoginFragment
import pl.solventive.localfarmer.main.mainContainer.container.HomeContainerFragment
import pl.solventive.localfarmer.main.mainContainer.drawer.HomeDrawerFragment
import pl.solventive.localfarmer.main.mainContainer.mainScreen.MainScreenFragment
import pl.solventive.localfarmer.main.mainContainer.mainScreen.home.HomeFragment
import pl.solventive.localfarmer.main.messages.MessagesFragment
import pl.solventive.localfarmer.main.postings.details.PostingDetailsFragment
import pl.solventive.localfarmer.main.postings.list.PostingsListFragment
import pl.solventive.localfarmer.main.profile.ProfileFragment
import pl.solventive.localfarmer.main.profile.edit.EditProfileFragment
import pl.solventive.localfarmer.main.profile.edit.localization.ProfileLocalizationFragment
import pl.solventive.localfarmer.main.profile.edit.photoPicker.ProfilePhotoPickerFragment
import pl.solventive.localfarmer.main.profile.edit.photoPicker.tabs.images.ProfilePhotoPickerImgFragment
import pl.solventive.localfarmer.main.profile.tabs.informations.ProfileInformationsFragment
import pl.solventive.localfarmer.main.profile.tabs.profileOpinions.ProfileOpinionsFragment
import pl.solventive.localfarmer.main.profile.tabs.profileProducts.ProfileProductsFragment
import pl.solventive.localfarmer.main.register.RegisterFragment
import pl.solventive.localfarmer.main.settings.SettingsFragment
import pl.solventive.localfarmer.main.splash.SplashFragment
import pl.solventive.localfarmer.util.components.imageCropper.ImageCropperFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashFragment

    @ContributesAndroidInjector
    abstract fun contributeMainScreenFragment(): MainScreenFragment

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeHomeFragmentContainer(): HomeContainerFragment

    @ContributesAndroidInjector
    abstract fun contributeHomeDrawer(): HomeDrawerFragment

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeRegisterFragment(): RegisterFragment

    @ContributesAndroidInjector
    abstract fun contributeAddPostingFragment(): AddPostingFragment

    @ContributesAndroidInjector
    abstract fun contributeMessagesFragment(): MessagesFragment

    @ContributesAndroidInjector
    abstract fun contributeFavouritesFragment(): FavouritesFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector
    abstract fun contributeHelpFragment(): HelpFragment

    @ContributesAndroidInjector
    abstract fun contributeInfoFragment(): InfoFragment

    @ContributesAndroidInjector
    abstract fun contributePhotoPickerImagesFragment(): PhotoPickerImagesFragment

    @ContributesAndroidInjector
    abstract fun contributeCategoryPickerFragment(): CategoryPickerFragment

    @ContributesAndroidInjector
    abstract fun contributePriceFragment(): PriceFragment

    @ContributesAndroidInjector
    abstract fun contributeQuantityFragment(): QuantityFragment

    @ContributesAndroidInjector
    abstract fun contributePostingDetailsFragment(): PostingDetailsFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileInformationsFragment(): ProfileInformationsFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileProductsFragment(): ProfileProductsFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileOpinionsFragment(): ProfileOpinionsFragment

    @ContributesAndroidInjector
    abstract fun contributeEditProfileFragment(): EditProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeProfilePhotoPicker(): ProfilePhotoPickerFragment

    @ContributesAndroidInjector
    abstract fun contributeProfilePhotoPickerImgFragment(): ProfilePhotoPickerImgFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileLocalizationFragment(): ProfileLocalizationFragment

    @ContributesAndroidInjector
    abstract fun contributeImageCropperFragment(): ImageCropperFragment

    @ContributesAndroidInjector
    abstract fun contributePostingsListFragment(): PostingsListFragment
}