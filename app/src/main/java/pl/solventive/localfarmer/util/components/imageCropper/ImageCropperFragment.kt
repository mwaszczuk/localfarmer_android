package pl.solventive.localfarmer.util.components.imageCropper

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.fragment_image_cropper.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.profile.edit.EditProfileViewModel
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class ImageCropperFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var editProfileViewModel: EditProfileViewModel

    private var callback: ImageCropperCallback? = null

    private var reqWidth = 0
    private var reqHeight = 0

    companion object {
        const val IMAGE_URI = "CROPPER_IMAGE_URI"
        const val IMAGE_TYPE = "CROPPER_IMAGE_TYPE"
        const val TYPE_PROFILE = 1
        const val TYPE_BACKGROUND = 2
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            callback = requireContext() as ImageCropperCallback
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val store = findNavController().getViewModelStoreOwner(R.id.editProfileGraph).viewModelStore
        editProfileViewModel =
            ViewModelProvider(store, viewModelFactory).get(EditProfileViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_image_cropper, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cropImageView.setImageUriAsync(requireArguments().getParcelable(IMAGE_URI))
        when (requireArguments().getInt(IMAGE_TYPE, -1)) {
            TYPE_PROFILE -> {
                cropImageView.cropShape = CropImageView.CropShape.OVAL
                cropImageView.setAspectRatio(1, 1)
                reqWidth = 200
                reqHeight = 200
            }
            TYPE_BACKGROUND -> {
                cropImageView.cropShape = CropImageView.CropShape.RECTANGLE
                cropImageView.setAspectRatio(10, 4)
                reqWidth = 1000
                reqHeight = 400
            }
        }

        btnSave.setOnClickListener {
            findNavController().navigate(ImageCropperFragmentDirections.goBackToProfileEdit())
            callback?.onImageCropped(cropImageView.getCroppedImage(reqWidth, reqHeight), requireArguments().getInt(IMAGE_TYPE, -1))
        }
    }

}