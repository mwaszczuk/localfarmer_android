package pl.solventive.localfarmer.util.components

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText

class NoSelectionEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : AppCompatEditText(context, attrs) {


    public override fun onSelectionChanged(start: Int, end: Int) {
        val text: CharSequence? = text
        if (text != null) {
            val textLength = if (text.isNotEmpty()) text.length - 3 else 0
            if (start != 0 + textLength || end != 0 + textLength) {
                setSelection(0 + textLength, 0 + textLength)
                return
            }
        }
        super.onSelectionChanged(start, end)
    }
}