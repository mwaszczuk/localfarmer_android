package pl.solventive.localfarmer.util.injection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pl.solventive.localfarmer.main.addPosting.AddPostingViewModel
import pl.solventive.localfarmer.main.addPosting.photoPicker.tabs.images.PhotoPickerImagesViewModel
import pl.solventive.localfarmer.main.login.LoginViewModel
import pl.solventive.localfarmer.main.mainContainer.container.HomeContainerViewModel
import pl.solventive.localfarmer.main.mainContainer.drawer.HomeDrawerViewModel
import pl.solventive.localfarmer.main.mainContainer.mainScreen.MainScreenViewModel
import pl.solventive.localfarmer.main.mainContainer.mainScreen.home.HomeViewModel
import pl.solventive.localfarmer.main.postings.details.PostingDetailsViewModel
import pl.solventive.localfarmer.main.postings.list.PostingsListViewModel
import pl.solventive.localfarmer.main.profile.ProfileViewModel
import pl.solventive.localfarmer.main.profile.edit.EditProfileViewModel
import pl.solventive.localfarmer.main.profile.edit.localization.ProfileLocalizationViewModel
import pl.solventive.localfarmer.main.profile.edit.photoPicker.tabs.images.ProfilePhotoPickerImgViewModel
import pl.solventive.localfarmer.main.profile.tabs.profileOpinions.ProfileOpinionsViewModel
import pl.solventive.localfarmer.main.profile.tabs.profileProducts.ProfileProductsViewModel
import pl.solventive.localfarmer.main.register.RegisterViewModel
import pl.solventive.localfarmer.main.splash.SplashViewModel

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainScreenViewModel::class)
    abstract fun bindMainScreenViewModel(viewModel: MainScreenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeContainerViewModel::class)
    abstract fun bindHomeContainerViewModel(viewModel: HomeContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeDrawerViewModel::class)
    abstract fun bindStartWindowViewModel(viewModel: HomeDrawerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    abstract fun bindRegisterViewModel(viewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddPostingViewModel::class)
    abstract fun bindAddPostingViewModel(viewModel: AddPostingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PhotoPickerImagesViewModel::class)
    abstract fun bindPhotoPickerImagesViewModel(viewModel: PhotoPickerImagesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostingDetailsViewModel::class)
    abstract fun bindPostingDetailsViewModel(viewModel: PostingDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileProductsViewModel::class)
    abstract fun bindProfileProductsViewModel(viewModel: ProfileProductsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileOpinionsViewModel::class)
    abstract fun bindProfileOpinionsViewModel(viewModel: ProfileOpinionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfilePhotoPickerImgViewModel::class)
    abstract fun bindProfilePhotoPickerImgViewModel(viewModel: ProfilePhotoPickerImgViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditProfileViewModel::class)
    abstract fun bindEditProfileViewModel(viewModel: EditProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileLocalizationViewModel::class)
    abstract fun bindProfileLocalizationViewModel(viewModel: ProfileLocalizationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostingsListViewModel::class)
    abstract fun bindPostingsListViewModel(viewModel: PostingsListViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: LocalFarmerViewModelFactory): ViewModelProvider.Factory
}