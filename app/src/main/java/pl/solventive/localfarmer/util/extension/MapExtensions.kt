package pl.solventive.localfarmer.util.extension

import android.content.Context
import androidx.core.content.ContextCompat
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import pl.solventive.localfarmer.R

fun MapView.addMarker(context: Context, point: GeoPoint?, onClickListener: Marker.OnMarkerClickListener? = null) {
    val marker = Marker(this)
    marker.position = point
    marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
    marker.icon = ContextCompat.getDrawable(context, R.drawable.ic_large_pin)
    if (onClickListener != null) marker.setOnMarkerClickListener(onClickListener)
    else marker.setOnMarkerClickListener { _, _ -> true }
    this.overlays.add(marker)
    this.invalidate()
}