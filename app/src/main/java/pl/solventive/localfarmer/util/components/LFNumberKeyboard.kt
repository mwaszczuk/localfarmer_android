package pl.solventive.localfarmer.util.components

import android.content.Context
import android.content.res.TypedArray
import android.text.InputFilter
import android.text.InputType
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.android.synthetic.main.component_lf_edit_text.view.*
import kotlinx.android.synthetic.main.component_lf_number_keyboard.view.*
import kotlinx.android.synthetic.main.component_lf_pin.view.*
import pl.solventive.localfarmer.R

/**
 * This is a custom LF Pin class that serves the purposes of displaying the PIN dots and handling the PIN input.
 *
 * @author  Marek Waszczuk
 * @version 1.0
 * @since   23.03.2020
 */

class LFNumberKeyboard @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs) {

    private val buttonClicked = MutableLiveData<Int>()

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.component_lf_number_keyboard, this, true)

        val buttons = listOf(btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnBackSpace)
        buttons.forEachIndexed { index, view ->
            view.setOnClickListener {
                if (index < 10) buttonClicked.value = index
                else buttonClicked.value = -1
            }
        }
    }

    /**
     * Returns a LiveData containing a value of last pressed button or -1 if the backspace button was clicked
     */
    fun observeKeyboardClicks(): LiveData<Int> = buttonClicked


}