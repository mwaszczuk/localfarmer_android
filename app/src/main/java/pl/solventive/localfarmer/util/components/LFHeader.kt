package pl.solventive.localfarmer.util.components

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.component_lf_header.view.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.util.extension.makeVisible

/**
 * This is a custom EditText wrapper class that combines EditText with other layout items.
 * It provides a nicely styled EditText View that can be easily used in the whole app using attributes in XML.
 *
 * @author  Marek Waszczuk
 * @version 1.0
 * @since   21.03.2020
 */

class LFHeader @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs) {

    private val alphaAnimation = AlphaAnimation(1.0f, 0.0f)
    var animDrawable: Int? = null
    var isErrorShown = false
    var isAccepted = false

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.component_lf_header, this, true)

        setupAlphaAnimation()

        val attrArray =
            context.obtainStyledAttributes(attrs, R.styleable.LFHeader, defStyle, defStyleRes)
        loadAttributes(attrArray)
    }

    /**
     * loads XML attributes.
     */
    private fun loadAttributes(attrs: TypedArray) {
        val headerText = attrs.getText(R.styleable.LFHeader_headerText)
        val description = attrs.getText(R.styleable.LFHeader_description)
        if (attrs.getBoolean(R.styleable.LFHeader_indicatorVisibility, false)) linearStatus.makeVisible()
        attrs.recycle()
        if (headerText != null) txtHeader.text = headerText
        if (description != null) txtDescription.text = description
    }

    private fun setupAlphaAnimation() {
        alphaAnimation.duration = 200
        alphaAnimation.repeatCount = 1
        alphaAnimation.repeatMode = Animation.REVERSE

        alphaAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationEnd(animation: Animation?) { }
            override fun onAnimationStart(animation: Animation?) { }
            override fun onAnimationRepeat(animation: Animation?) {
                if (animDrawable != null)
                    imgStatus.setImageDrawable(ContextCompat.getDrawable(context, animDrawable!!))
            }
        })
    }

    fun showIndicator(drawableRes: Int) {
        if (animDrawable != drawableRes) {
            animDrawable = drawableRes
            imgStatus.startAnimation(alphaAnimation)
        }
    }

    fun hideIndicator() {
        showIndicator(R.drawable.ic_empty_circle)
    }

    /**
     * Lights the header on red.
     */
    fun showError() {
        isErrorShown = true
        showIndicator(R.drawable.ic_error_circle)
        txtHeader.setTextColor(ContextCompat.getColor(context, R.color.red))
        val anim = AnimationUtils.loadAnimation(context, R.anim.shake)
        this.startAnimation(anim)
    }

    /**
     * shows green icon in the component.
     */
    fun showAccepted() {
        showIndicator(R.drawable.ic_success_circle)
        isAccepted = true
    }

    /**
     * Hides error
     */
    fun hideError() {
        isErrorShown = false
    }
}
