package pl.solventive.localfarmer.util.components

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.appcompat.widget.AppCompatImageView
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.util.extension.makeGone
import pl.solventive.localfarmer.util.extension.makeVisible

class CarrotProgressBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : AppCompatImageView(context, attrs), Animator.AnimatorListener {

    private var animator1 = ObjectAnimator()
    private var animator2 = ObjectAnimator()
    private var animationCounter = 2

    init {
        this.setImageResource(R.drawable.ic_carrot)
        initFlipAnimation()
        startAnimation()
    }

    override fun onAnimationStart(p0: Animator?) {}
    override fun onAnimationRepeat(p0: Animator?) {}
    override fun onAnimationCancel(p0: Animator?) {}

    override fun onAnimationEnd(p0: Animator?) {
        if(p0 == animator1) {
            if((animationCounter % 2) == 0){
                this.setImageResource(R.drawable.ic_carrot_inverted)
            } else {
                this.setImageResource(R.drawable.ic_carrot)
            }
            animationCounter++
            animator2.start()
        } else {
            startAnimation()
        }
    }

    private fun initFlipAnimation() {
        animator1 = ObjectAnimator.ofFloat(this, "scaleX", 1f, 0f)
        animator2 = ObjectAnimator.ofFloat(this, "scaleX", 0f, 1f)
        animator1.duration = 300
        animator2.duration = 300
        animator1.interpolator = LinearInterpolator()
        animator2.interpolator = LinearInterpolator()
        animator1.addListener(this)
        animator2.addListener(this)
    }

    private fun startAnimation() {
        if(this.visibility == View.VISIBLE) {
            animator1.start()
        }
    }

    fun show(minTime: Int? = null) {
        this.makeVisible()
        animator1.start()
    }

    fun hide() {
        this.makeGone()
        animator1.cancel()
        animator2.cancel()
    }

}
