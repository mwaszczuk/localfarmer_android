package pl.solventive.localfarmer.util.extension

import android.content.Context
import org.joda.time.DateTime
import pl.solventive.localfarmer.R

/**
 * Returns this day with time set to 00:00:00.
 */
fun DateTime.startOfDay(): DateTime
        = withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)

/**
 * Returns this day with time set to 23:59:59.
 */
fun DateTime.endOfDay(): DateTime
        = withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999)

/**
 * Returns the day at the beginning of this week.
 */
fun DateTime.startOfWeek(): DateTime
        = withDayOfWeek(1)

/**
 * Returns the day at the end of this week.
 */
fun DateTime.endOfWeek(): DateTime
        = withDayOfWeek(7)

/**
 * Returns the day at the beginning of this month.
 */
fun DateTime.startOfMonth(): DateTime
        = withDayOfMonth(1)

/**
 * Returns the day at the end of this month.
 */
fun DateTime.endOfMonth(): DateTime
        = plusMonths(1).withDayOfMonth(1).minusDays(1)

/**
 * Returns number of days in this month.
 */
fun DateTime.daysInMonth(): Int
        = plusMonths(1).withDayOfMonth(1).minusDays(1).dayOfMonth

fun DateTime.toApiString() = this.toString("yyyy-MM-dd'T'HH:mm:ss")

fun DateTime.toPrettyDateTimeString(context: Context?): String {
    return context?.let { c ->
        val now = DateTime.now().startOfDay()
        val date = DateTime(year, monthOfYear, dayOfMonth, 0, 0)

        val sameDay = date == now
        val previousDay = date == now.minusDays(1)
        val nextDay = date == now.plusDays(1)
        val sameYear = date.year == now.year

        when {
            sameDay -> c.getString(R.string.date_today, toString("HH:mm"))
            previousDay -> c.getString(R.string.date_yesterday, toString("HH:mm"))
            nextDay -> c.getString(R.string.date_tomorrow, toString("HH:mm"))
            sameYear -> c.getString(R.string.date_older_than_yesterday, toString("dd MMM"), toString("HH:mm"))
            else -> c.getString(R.string.date_older_than_yesterday, toString("dd MMM yyyy"), toString("HH:mm"))
        }
    } ?: ""
}