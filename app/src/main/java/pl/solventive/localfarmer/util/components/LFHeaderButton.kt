package pl.solventive.localfarmer.util.components

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.component_lf_header_button.view.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.util.extension.makeVisible

/**
 * A button with an indicator that we can open some view on the right.
 *
 * @author  Marek Waszczuk
 * @version 1.0
 * @since   04.04.2020
 */

class LFHeaderButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs) {

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.component_lf_header_button, this, true)

        val attrArray =
            context.obtainStyledAttributes(attrs, R.styleable.LFHeaderButton, defStyle, defStyleRes)
        loadAttributes(attrArray)
    }

    /**
     * loads XML attributes.
     */
    private fun loadAttributes(attrs: TypedArray) {
        val text = attrs.getText(R.styleable.LFHeaderButton_text)
        val headerText = attrs.getText(R.styleable.LFHeaderButton_headerText)
        val description = attrs.getText(R.styleable.LFHeaderButton_description)
        val hint = attrs.getText(R.styleable.LFHeaderButton_hint)
        if (attrs.getBoolean(R.styleable.LFHeaderButton_indicatorVisibility, false)) linearStatus.makeVisible()
        attrs.recycle()
        if (text != null) btnText.text = text
        if (hint != null) btnText.hint = hint
        if (headerText != null) txtHeader.text = headerText
        if (description != null) txtDescription.text = description
    }

    fun setText(text: String) {
        btnText.text = text
    }

    override fun setOnClickListener(l: OnClickListener?) {
        buttonLayout.setOnClickListener(l)
    }
}
