package pl.solventive.localfarmer.util.components

import com.google.android.material.appbar.AppBarLayout
import kotlin.math.abs

/**
 * A listener for app bar state changes.
 * Used to detect whether the collapsing toolbar is collapsed or expanded.
 *
 * @param offsetTopMargin - Top margin in px for detecting app bar collapsed.
 * The bigger this value the earlier collapsed state will be detected.
 *
 * @author  Marek Waszczuk
 * @version 1.0.01
 * @since   18.04.2020
 */
abstract class AppBarStateChangeListener(
    private val offsetTopMargin: Int = 0
) : AppBarLayout.OnOffsetChangedListener {

    enum class State {
        EXPANDED, COLLAPSED, IDLE
    }

    private var mCurrentState = State.IDLE

    override fun onOffsetChanged(appBarLayout: AppBarLayout, i: Int) {
        if (i == 0 && mCurrentState != State.EXPANDED) {
            onStateChanged(appBarLayout, State.EXPANDED)
            mCurrentState = State.EXPANDED
        }
        else if (abs(i - offsetTopMargin) >= appBarLayout.totalScrollRange && mCurrentState != State.COLLAPSED) {
            onStateChanged(appBarLayout, State.COLLAPSED)
            mCurrentState = State.COLLAPSED
        }
        else if (mCurrentState != State.IDLE) {
            if (offsetTopMargin == 0 || (appBarLayout.totalScrollRange - abs(i)) > offsetTopMargin) {
                onStateChanged(appBarLayout, State.IDLE)
                mCurrentState = State.IDLE
            }
        }
    }

    abstract fun onStateChanged(
        appBarLayout: AppBarLayout?,
        state: State?
    )
}