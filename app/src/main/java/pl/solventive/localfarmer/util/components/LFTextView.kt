package pl.solventive.localfarmer.util.components

import android.content.Context
import android.util.AttributeSet
import android.view.animation.AlphaAnimation
import android.view.animation.Animation

/**
 * This is a custom TextView class with additional functionalities.
 *
 * @author  Marek Waszczuk
 * @version 1.0
 * @since   23.03.2020
 */

class LFTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : androidx.appcompat.widget.AppCompatTextView(context, attrs) {

    private val alphaAnimation = AlphaAnimation(1.0f, 0.0f)
    private var animText = ""

    init {
        alphaAnimation.duration = 200
        alphaAnimation.repeatCount = 1
        alphaAnimation.repeatMode = Animation.REVERSE

        alphaAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationEnd(animation: Animation?) { }
            override fun onAnimationStart(animation: Animation?) { }
            override fun onAnimationRepeat(animation: Animation?) { text = animText }
        })
    }

    /**
     * Changes to a given text with alpha out - alpha in animation
     */
    fun changeText(text: String) {
        animText = text
        this.startAnimation(alphaAnimation)
    }

}
