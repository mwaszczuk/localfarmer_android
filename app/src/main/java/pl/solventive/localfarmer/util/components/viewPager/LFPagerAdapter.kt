package pl.solventive.localfarmer.util.components.viewPager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import timber.log.Timber

open class LFPagerAdapter(fm: FragmentManager
) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val items = arrayListOf<Fragment>()
    private val titles = arrayListOf<String>()

    override fun getPageTitle(position: Int) = titles[position]
    override fun getItem(position: Int) = items[position]
    override fun getCount(): Int = items.size

    fun addFragment(fragment: Fragment, title: String) {
        if (!titles.contains(title)) {
            items.add(fragment)
            titles.add(title)
        } else {
            Timber.tag(this.javaClass.name).w("A fragment with this title already exists in this adapter: $title")
        }
    }
}