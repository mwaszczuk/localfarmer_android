package pl.solventive.localfarmer.util.components

import android.content.Context
import android.net.*
import androidx.lifecycle.LiveData
import pl.solventive.localfarmer.LocalFarmerApplication

class ConnectionLiveData(
    private val application: LocalFarmerApplication)
    : LiveData<Boolean>()
{

    private var networkRequest: NetworkRequest = NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .build()

    override fun onActive() {
        super.onActive()
        getDetails()
    }

    private fun getDetails() {
        val cm = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.registerNetworkCallback(networkRequest, object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network?) {
                super.onAvailable(network)
                postValue(true)
            }

            override fun onUnavailable() {
                super.onUnavailable()
                postValue(false)
            }

            override fun onLost(network: Network?) {
                super.onLost(network)
                postValue(false)
            }
        })
    }
}