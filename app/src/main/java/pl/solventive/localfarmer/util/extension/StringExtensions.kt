package pl.solventive.localfarmer.util.extension

import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import pl.solventive.localfarmer.BuildConfig

fun String.toGlideUrl(token: String) =
    GlideUrl(
        BuildConfig.API_ADDRESS.plus("v1/files/downloadFile/").plus(this),
        LazyHeaders.Builder()
            .addHeader("Authorization", token)
            .build()
    )