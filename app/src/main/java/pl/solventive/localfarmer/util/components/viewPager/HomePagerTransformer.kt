package pl.solventive.localfarmer.util.components.viewPager

import android.view.View
import androidx.viewpager.widget.ViewPager
import kotlin.math.abs
import kotlin.math.max

class HomePagerTransformer : ViewPager.PageTransformer {

    private val minScale = 0.65f

    override fun transformPage(page: View, position: Float) {
        val scaleFactorX = max(minScale, 1 - (abs(position) / 1.5f))
        val scaleFactorY = max(minScale, 1 - (abs(position) / 1.8f))
        if (position > 0) {
            page.scaleX = scaleFactorX
            page.scaleY = scaleFactorY
        }

    }
}