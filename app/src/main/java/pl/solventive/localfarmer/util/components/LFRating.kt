package pl.solventive.localfarmer.util.components

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import pl.solventive.localfarmer.R

class LFRating @JvmOverloads constructor(
    context: Context,
    private val attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    enum class Star {
        Empty, Half, Full
    }

    private var starsInflated = 0

    init {
        val layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        this.orientation = HORIZONTAL
        this.layoutParams = layoutParams
    }

    fun setValue(value: Double) {
        val fullStars = value.toInt()
        val halfStars = if ((value - fullStars) > 0.7 ) 1 else 0
        val emptyStars = 5 - (fullStars + halfStars)

        for (x in 0 until fullStars) addStar(Star.Full)
        for (x in 0 until halfStars) addStar(Star.Half)
        for (x in 0 until emptyStars) addStar(Star.Empty)
    }

    private fun addStar(star: Star) {
        if (starsInflated < 5) {
            when (star) {
                Star.Empty -> { addView(createStar(R.drawable.ic_star_border_24dp)) }
                Star.Half -> { addView(createStar(R.drawable.ic_star_half_24dp)) }
                Star.Full -> { addView(createStar(R.drawable.ic_star_24dp)) }
            }
        }
        starsInflated++
    }

    private fun getStarParams(): LayoutParams
        = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)


    private fun createStar(drawableId: Int): ImageView {
        val image = ImageView(context, attrs)
        image.layoutParams = getStarParams()
        image.setImageDrawable(ContextCompat.getDrawable(context, drawableId))
        return image
    }

    private fun convertDpToPx(dp: Float) =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).toInt()


}