package pl.solventive.localfarmer.util.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.solventive.localfarmer.main.addPosting.photoPicker.PhotoPickerFragment

@Suppress("unused")
@Module
abstract class DialogModule {

    @ContributesAndroidInjector
    abstract fun contributePhotoPickerDialog(): PhotoPickerFragment
}