package pl.solventive.localfarmer.util.components

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import pl.solventive.localfarmer.R

class LFDialog(
      caller: Fragment,
      root: ViewGroup
) {

    var dialog: AlertDialog? = null
    var dialogView: View? = null
    var autoDismiss = false

    init {
        val alertBuilder = AlertDialog.Builder(caller.requireContext(), R.style.LFDialog)
        dialogView = caller.layoutInflater.inflate(R.layout.component_lf_dialog, root, false)
        alertBuilder.setView(dialogView)
        dialog = alertBuilder.create()
    }


    fun positiveText(text: String): LFDialog {
        dialogView?.findViewById<Button>(R.id.buttonPositive)?.let {
            if (text.length > 6) it.textSize = 20f
            it.text = text
        }

        return this
    }

    fun onPositive(callback: () -> Unit): LFDialog {
        dialogView?.findViewById<Button>(R.id.buttonPositive)?.setOnClickListener {
            callback.invoke()
            if (autoDismiss) dialog?.dismiss()
        }
        return this
    }

    fun negativeText(text: String): LFDialog {
        dialogView?.findViewById<Button>(R.id.buttonNegative)?.let {
            if (text.length > 6) it.textSize = 20f
            it.text = text
        }
        return this
    }

    fun onNegative(callback: () -> Unit): LFDialog {
        dialogView?.findViewById<Button>(R.id.buttonNegative)?.setOnClickListener {
            callback.invoke()
            if (autoDismiss) dialog?.dismiss()
        }
        return this
    }

    fun title(text:String): LFDialog {
        dialogView?.findViewById<TextView>(R.id.txtTitle)?.text = text
        return this
    }

    fun content(text: String): LFDialog {
        dialogView?.findViewById<TextView>(R.id.txtContent)?.text = text
        return this
    }

    fun show() {
        dialog?.show()
    }

    fun dismiss() {
        dialog?.dismiss()
    }

    fun autoDismiss(b: Boolean) : LFDialog {
        autoDismiss = b
        dialogView?.findViewById<Button>(R.id.buttonPositive)?.let {
            if (!it.hasOnClickListeners()) it.setOnClickListener { dialog?.dismiss() }
        }
        dialogView?.findViewById<Button>(R.id.buttonNegative)?.let {
            if (!it.hasOnClickListeners()) it.setOnClickListener { dialog?.dismiss() }
        }
        return this
    }

}