package pl.solventive.localfarmer.main.addPosting.categoryPicker

import pl.solventive.localfarmer.data.database.entity.ProductCategory
import pl.solventive.localfarmer.data.database.entity.ProductUnit
import pl.solventive.localfarmer.data.network.dto.DictionaryEntryDto

data class CategoryPickerModel(
    val id: Int,
    val name: String,
    val apiName: String,
    var isChecked: Boolean
) {
    constructor(dictionaryEntry: DictionaryEntryDto): this(
        dictionaryEntry.id,
        dictionaryEntry.name,
        dictionaryEntry.apiName,
        false
    )

    constructor(productUnit: ProductUnit): this(
        productUnit.id,
        productUnit.name,
        productUnit.apiName,
        false
    )

    constructor(productCategory: ProductCategory): this(
        productCategory.id,
        productCategory.name,
        productCategory.apiName,
        false
    )
}