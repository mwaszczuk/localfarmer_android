package pl.solventive.localfarmer.main.addPosting.price

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_category_picker.toolbar
import kotlinx.android.synthetic.main.fragment_product_price.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.addPosting.AddPostingViewModel
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class PriceFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: AddPostingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val store = findNavController().getViewModelStoreOwner(R.id.addPostingGraph).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(AddPostingViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_product_price, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        edtPrice.setText(" zł")
        if (viewModel.price != null) edtPrice.setText("%.2f".format(viewModel.price).replace(",", ".").plus(" zł"))
        txtUnit.text = ("/").plus(viewModel.selectedQuantityUnit?.name ?: "")

        edtPrice.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.price = s.toString().dropLast(3).toDoubleOrNull()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        if (edtPrice.requestFocus()) {
            (edtPrice.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).toggleSoftInput(0, 0)
        }
    }
}