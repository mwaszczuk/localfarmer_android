package pl.solventive.localfarmer.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import io.reactivex.disposables.CompositeDisposable
import pl.solventive.localfarmer.R

open class LFFragment : Fragment() {

    protected lateinit var disposable: CompositeDisposable

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        disposable = CompositeDisposable()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable.dispose()
    }

    fun findNavController() : NavController = requireActivity().findNavController(R.id.nav_host_fragment)

}