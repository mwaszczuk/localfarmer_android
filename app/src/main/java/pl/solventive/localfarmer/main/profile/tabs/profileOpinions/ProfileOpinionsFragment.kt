package pl.solventive.localfarmer.main.profile.tabs.profileOpinions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.to_do.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.util.extension.makeGone
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class ProfileOpinionsFragment() : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: ProfileOpinionsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ProfileOpinionsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.to_do, container, false)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topBar.makeGone()
    }

}
