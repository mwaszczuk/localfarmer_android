package pl.solventive.localfarmer.main.mainContainer.mainScreen.home

import pl.solventive.localfarmer.data.database.entity.ProductUnit
import pl.solventive.localfarmer.data.network.dto.PostingDto

data class PostingListModel(
    val unit: ProductUnit,
    val dto: PostingDto
)