package pl.solventive.localfarmer.main.profile.edit

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.webkit.MimeTypeMap
import androidx.lifecycle.MutableLiveData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.apache.commons.io.FileUtils
import org.osmdroid.util.GeoPoint
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadLocationDto
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadUserDto
import pl.solventive.localfarmer.data.network.repository.FileRepository
import pl.solventive.localfarmer.data.network.repository.LocationRepository
import pl.solventive.localfarmer.data.network.repository.UserRepository
import pl.solventive.localfarmer.main.LFViewModel
import pl.solventive.localfarmer.util.Event
import pl.solventive.localfarmer.util.components.imageCropper.ImageCropperFragment
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class EditProfileViewModel @Inject constructor(
    private val prefs: SharedPreferences,
    private val fileRepo: FileRepository,
    private val userRepo: UserRepository,
    private val locationRepo: LocationRepository
) : LFViewModel() {

    var tempPermissionImageType = 0
    var userId = -1

    val isLoading = MutableLiveData<Event<Boolean>>()
    val posting = BehaviorSubject.create<PostingDto>()
    val editSuccess = BehaviorSubject.create<Boolean>()

    val profilePhoto = MutableLiveData<Bitmap>()
    val backgroundPhoto = MutableLiveData<Bitmap>()
    var profilePhotoByteArray: ByteArray? = null
    var backgroundPhotoByteArray: ByteArray? = null
    var name = ""
    var surname = ""
    var description = ""
    var city: String? = null
    var buildingNumber: String? = null
    var point: GeoPoint? = null

    fun saveAndQuit(context: Context) {
        isLoading.value = Event(true)
        saveProfile(context, profilePhotoByteArray, backgroundPhotoByteArray)
    }

    private fun saveProfile(context: Context, profilePhoto: ByteArray?, backgroundPhoto: ByteArray?) {
        val profileMultipart = if (profilePhoto != null) createMultipart(context, profilePhoto) else null
        val backgroundMultipart = if (backgroundPhoto != null) createMultipart(context, backgroundPhoto) else null

        disposable.add(
            Single.just(Pair(profileMultipart, backgroundMultipart))
                .flatMap { files ->
                    if (files.first != null) {
                        fileRepo.postFile(files.first!!)
                            .map { Pair(it.id, files.second) }
                    } else Single.just(Pair(null, files.second))
                }
                .flatMap { files ->
                    if (files.second != null) {
                        fileRepo.postFile(files.second!!)
                            .map { listOf(files.first, it.id) }
                    } else Single.just(listOf(files.first, null))
                }
                .flatMap { files ->
                    if (point != null && city != null) {
                        locationRepo.addLocation(createLocation())
                            .map { Pair(files, it.id) }
                    } else Single.just(Pair(files, null))
                }
                .subscribeOn(Schedulers.io())
                .subscribe({ uploadProfileInfo(it) }, {Timber.tag("ERROR").d(it)})
        )
    }

    private fun createMultipart(context: Context, photo: ByteArray): MultipartBody.Part? {
        val imageFile = saveTemporaryFile(context, photo)
        val mimeType = MimeTypeMap.getSingleton()
            .getMimeTypeFromExtension(FileUtils.getExtension(imageFile.name)) ?: return null
        val requestFile = imageFile.asRequestBody(mimeType.toMediaTypeOrNull())
        return MultipartBody.Part.createFormData("file", imageFile.name, requestFile)
    }

    private fun saveTemporaryFile(context: Context, bytes: ByteArray): File {
        val tmpFile = File(context.cacheDir, "image.png")
        if (tmpFile.exists()) {
            tmpFile.delete()
        }

        tmpFile.createNewFile()

        val fileOutputStream = FileOutputStream(tmpFile)
        fileOutputStream.write(bytes)
        fileOutputStream.flush()
        fileOutputStream.close()

        return tmpFile
    }

    private fun uploadProfileInfo(data: Pair<List<String?>, String?>) {
        val uploadDto = UploadUserDto(
            profilePhotoId = data.first[0],
            backgroundPhotoId = data.first[1],
            locationId = data.second,
            name = if (name.isEmpty()) null else name,
            surname = if (surname.isEmpty()) null else surname,
            description = if (description.isEmpty()) null else description
        )
        disposable.add(
            userRepo.putUser(userId, uploadDto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ editSuccess.onNext(true) }, {editSuccess.onNext(false)})
        )
    }

    private fun createLocation() =
        UploadLocationDto(
            name = "LocalFarmerLocation",
            userId = userId,
            city = city!!,
            buildingNumber = buildingNumber,
            latitude = point!!.latitude,
            longitude = point!!.longitude
        )

    fun onImageCropped(image: Bitmap, imageType: Int) {
        Timber.tag("LocalFarmer").d("Image cropped!: ${image.byteCount} B")
        val quality = if (image.byteCount > 500000) 50 else 100
        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, quality, stream)
        val photo = stream.toByteArray()
        Timber.tag("LocalFarmer").d("Image compressed!: ${photo.size} B")
        when (imageType) {
            ImageCropperFragment.TYPE_PROFILE -> {
                profilePhotoByteArray = photo
                profilePhoto.value = image
            }
            ImageCropperFragment.TYPE_BACKGROUND -> {
                backgroundPhotoByteArray = photo
                backgroundPhoto.value = image
            }
        }
    }

}