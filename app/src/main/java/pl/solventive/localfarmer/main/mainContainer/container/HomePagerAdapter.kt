package pl.solventive.localfarmer.main.mainContainer.container

import androidx.fragment.app.FragmentManager
import pl.solventive.localfarmer.util.components.viewPager.LFPagerAdapter

class HomePagerAdapter(fm: FragmentManager) : LFPagerAdapter(fm)