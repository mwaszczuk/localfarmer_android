package pl.solventive.localfarmer.main.addPosting.categoryPicker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_category_picker.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.addPosting.AddPostingViewModel
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class CategoryPickerFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: AddPostingViewModel

    private val adapter by lazy {CategoryPickerAdapter(::onItemClicked)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val store = findNavController().getViewModelStoreOwner(R.id.addPostingGraph).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(AddPostingViewModel::class.java)
        adapter.items = viewModel.productCategories
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
         = inflater.inflate(R.layout.fragment_category_picker, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        rvCategories.adapter = adapter
    }

    private fun onItemClicked(category: CategoryPickerModel, position: Int) {
        if (!viewModel.productCategories[position].isChecked) {
            if (viewModel.selectedCategory != null) viewModel.productCategories[viewModel.selectedCategory!!.id - 1].isChecked = false
            viewModel.productCategories[position].isChecked = true
            viewModel.selectedCategory = category
        } else {
            viewModel.productCategories[position].isChecked = false
            viewModel.selectedCategory = null
        }
        findNavController().navigateUp()
    }
}