package pl.solventive.localfarmer.main.profile.edit.photoPicker.tabs.images

import java.io.File
import java.util.*

data class ProfilePhotoPickerImgModel(
        val id: UUID,
        val file: File,
        var isChecked: Boolean
)