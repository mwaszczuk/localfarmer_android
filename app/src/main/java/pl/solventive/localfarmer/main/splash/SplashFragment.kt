package pl.solventive.localfarmer.main.splash

import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.FrameLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_splash.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.util.injection.Injectable
import timber.log.Timber
import javax.inject.Inject

class SplashFragment : LFFragment(), Injectable {

    private val handler = Handler()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: SplashViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(SplashViewModel::class.java)

        viewModel.loginSuccess.observe(viewLifecycleOwner, Observer {
            if (!it) {
                // showError
            } else {
                if (viewModel.isHandlerWaiting.value == false) {
                    Timber.tag("LocalFarmer").d("Animating from login observer, because login succeeded after handler time was over")
                    animateLogoOut(logoImg, 1f, 18f)
                }
            }
        })

        viewModel.isHandlerWaiting.observe(viewLifecycleOwner, Observer {
            if (it) handler.postDelayed({
                Timber.tag("LocalFarmer").d("handler time is over")
                viewModel.isHandlerWaiting.value = false
                if (viewModel.loginSuccess.value == true) {
                    Timber.tag("LocalFarmer").d("Animating from handler observer, because login has already succeeded")
                    animateLogoOut(logoImg, 1f, 18f)
                }
            }, 1500)
        })

        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val imageHeight = (displayMetrics.heightPixels * 0.8).toInt()
        val imageWidth = (displayMetrics.widthPixels * 0.8).toInt()
        val lp = FrameLayout.LayoutParams(imageWidth, imageHeight)
        lp.setMargins((displayMetrics.widthPixels - imageWidth) / 2, (displayMetrics.heightPixels - imageHeight) / 3,
            0, 0)
        logoImg.layoutParams = lp
        Timber.tag("LocalFarmer").d("Handler waiting")
        viewModel.isHandlerWaiting.value = true
    }

    private fun animateLogoOut(view: View, startScale: Float, endScale: Float) {
        val anim = ScaleAnimation(
            startScale, endScale,
            startScale, endScale,
            Animation.RELATIVE_TO_SELF, 0.185f,
            Animation.RELATIVE_TO_SELF, 0.52f)
        anim.fillAfter = true
        anim.duration = 700
        view.startAnimation(anim)
        anim.setAnimationListener(object: Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {}
            override fun onAnimationRepeat(p0: Animation?) {}
            override fun onAnimationEnd(p0: Animation?) { findNavController().navigate(SplashFragmentDirections.openHomeFragment()) }
        })
    }

}