package pl.solventive.localfarmer.main.profile.edit.photoPicker.tabs.images

import io.reactivex.subjects.BehaviorSubject
import pl.solventive.localfarmer.main.LFViewModel
import java.io.File
import java.util.*
import javax.inject.Inject

class ProfilePhotoPickerImgViewModel @Inject constructor() : LFViewModel() {

    val images = BehaviorSubject.createDefault(emptyList<ProfilePhotoPickerImgModel>())

    fun addImages(files: List<File>, selectedFiles: List<File>) {
        images.onNext(
                files.map { ProfilePhotoPickerImgModel(UUID.randomUUID(), it, selectedFiles.contains(it)) }
                        .sortedByDescending { it.file.lastModified() }
        )
    }

}