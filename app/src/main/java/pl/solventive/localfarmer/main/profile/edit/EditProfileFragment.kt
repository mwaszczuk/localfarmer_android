package pl.solventive.localfarmer.main.profile.edit

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.util.components.imageCropper.ImageCropperFragment
import pl.solventive.localfarmer.util.extension.makeInvisible
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class EditProfileFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: EditProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val store = findNavController().getViewModelStoreOwner(R.id.editProfileGraph).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(EditProfileViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_edit_profile, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disposable.addAll(
            viewModel.posting.subscribe { showSuccess() },
            viewModel.editSuccess.subscribe {
                if (it) findNavController().navigateUp()
                else Toast.makeText(requireContext(), "Wystąpił błąd", Toast.LENGTH_SHORT).show() }
        )

        viewModel.userId = arguments?.getInt("userId") ?: -1

        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }

        observeLiveDatas()
        bindViews()

        edtName.observeTextChanges().observe(viewLifecycleOwner, Observer { viewModel.name = it })
        edtSurname.observeTextChanges().observe(viewLifecycleOwner, Observer { viewModel.surname = it })
        edtDescription.observeTextChanges().observe(viewLifecycleOwner, Observer { viewModel.description = it })
        edtName.setText(viewModel.name, true)
        edtSurname.setText(viewModel.surname, true)
        edtDescription.setText(viewModel.description, true)

        btnLocalization.setOnClickListener { findNavController().navigate(EditProfileFragmentDirections.openLocalization()) }
        profilePhotoLayout.setOnClickListener { checkPermissions(ImageCropperFragment.TYPE_PROFILE) }
        backgroundPhotoLayout.setOnClickListener { checkPermissions(ImageCropperFragment.TYPE_BACKGROUND) }
        btnConfirm.setOnClickListener { viewModel.saveAndQuit(requireContext()) }

    }

    private fun observeLiveDatas() {
        viewModel.profilePhoto.observe(viewLifecycleOwner, Observer {
            imgAddProfilePhoto.makeInvisible()
            Glide.with(this)
                .load(it)
                .centerCrop()
                .apply(RequestOptions.circleCropTransform())
                .into(imgProfilePhoto)
        })
        viewModel.backgroundPhoto.observe(viewLifecycleOwner, Observer {
            Glide.with(this)
                .load(it)
                .centerCrop()
                .into(imgAddBackgroundPhoto)
        })
        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { b ->
                if (b) {
                    scrollView.animate().alpha(0.2f).duration = 200
                    carrot.show()
                } else {
                    scrollView.animate().alpha(1f).duration = 200
                    carrot.hide()
                }
            }
        })
    }

    private fun bindViews() {
        viewModel.city?.let { btnLocalization.setText(it) }
    }

    private fun checkPermissions(imageType: Int) {
        if (checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            viewModel.tempPermissionImageType = imageType
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_EXTERNAL_STORAGE)) { }
            requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                1234
            )
            return
        } else findNavController().navigate(EditProfileFragmentDirections.openPhotoPicker(imageType))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1234) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                findNavController().navigate(EditProfileFragmentDirections.openPhotoPicker(viewModel.tempPermissionImageType))
            }
        }
    }

    private fun showError() {
        Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()
    }

    private fun showSuccess() {

    }

    fun onImageCropped(image: Bitmap, imageType: Int) {
        viewModel.onImageCropped(image, imageType)
    }
}