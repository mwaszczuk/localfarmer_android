package pl.solventive.localfarmer.main.login.steps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_register_verify_number.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.login.LoginViewModel

class LoginVerifyNumberFragment(
    private val parentViewModel: LoginViewModel
) : LFFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login_verify_number, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        parentViewModel.codeVerificationStatus.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { b ->
                if (!b) edtVerifyingCode.showError(resources.getString(R.string.fragment_login_code_error))
            }
        })
        edtVerifyingCode.observeTextChanges().observe(viewLifecycleOwner, Observer { parentViewModel.inputVerifyingCode.value = it })
    }
}