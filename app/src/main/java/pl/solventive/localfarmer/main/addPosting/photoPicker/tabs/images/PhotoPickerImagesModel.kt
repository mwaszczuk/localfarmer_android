package pl.solventive.localfarmer.main.addPosting.photoPicker.tabs.images

import java.io.File
import java.util.*

data class PhotoPickerImagesModel(
        val id: UUID,
        val file: File,
        var isChecked: Boolean
)