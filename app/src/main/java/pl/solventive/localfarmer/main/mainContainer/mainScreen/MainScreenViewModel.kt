package pl.solventive.localfarmer.main.mainContainer.mainScreen

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.main.LFViewModel
import javax.inject.Inject

class MainScreenViewModel @Inject constructor(
    private val appStateRepository: AppStateRepository
) : LFViewModel() {

    var view: View? = null

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    fun isUserLoggedIn() = appStateRepository.getUserId() > 0
}