package pl.solventive.localfarmer.main.profile.edit.photoPicker.tabs.images

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_photo_picker_images.view.*
import pl.solventive.localfarmer.R

class ProfilePhotoPickerImgAdapter(
        val onClick: ((ProfilePhotoPickerImgModel) -> Unit)?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val picasso = Picasso.get()
    var items = listOf<ProfilePhotoPickerImgModel>()

    fun itemComparator(oldItem: ProfilePhotoPickerImgModel, newItem: ProfilePhotoPickerImgModel) = oldItem.id == newItem.id

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CounterpartyViewHolder
            = CounterpartyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_photo_picker_images, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CounterpartyViewHolder) holder.bind(items[holder.adapterPosition])
    }

    inner class CounterpartyViewHolder(view: View)
        : RecyclerView.ViewHolder(view) {

        fun bind(item: ProfilePhotoPickerImgModel) {
            itemView.img.setOnClickListener {
                onClick?.invoke(item)
            }
            picasso.load(item.file)
                    .resize(512, 512)
                    .centerCrop()
                    .into(itemView.img)

            animateCheck(true, item.isChecked)
        }

        private fun animateCheck(instant: Boolean, show: Boolean) {
            itemView.imgCheck.animate()
                    .alpha(if (show) 1f else 0f)
                    .setDuration(if (instant) 0 else 200)
            itemView.imgFrameChecked.animate()
                .alpha(if (show) 1f else 0f)
                .setDuration(if (instant) 0 else 200)

        }

    }
}