package pl.solventive.localfarmer.main.addPosting.photoPicker.tabs.camera

//import android.content.Intent
//import android.graphics.Bitmap
//import android.os.Bundle
//import android.os.Environment
//import android.provider.MediaStore
//import android.util.Log
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.lifecycle.ViewModelProvider
//import androidx.navigation.fragment.findNavController
//import io.fotoapparat.Fotoapparat
//import io.fotoapparat.configuration.CameraConfiguration
//import io.fotoapparat.parameter.ScaleType
//import io.fotoapparat.result.adapter.rxjava2.toSingle
//import io.fotoapparat.selector.*
//import io.reactivex.Single
//import io.reactivex.android.schedulers.AndroidSchedulers
//import io.reactivex.schedulers.Schedulers
//import kotlinx.android.synthetic.main.view_chat_attachments_camera.*
//import org.joda.time.DateTime
//import pl.itcenter.sotmanager.R
//import pl.itcenter.sotmanager.presentation.BaseFragment
//import pl.itcenter.sotmanager.presentation.chat.ChatViewModel
//import pl.itcenter.sotmanager.util.injection.Injectable
//import java.io.File
//import java.io.FileOutputStream
//import java.lang.Exception
//import javax.inject.Inject
//
//class ChatAttachmentsCameraView : BaseFragment(), Injectable {
//
//    @Inject
//    lateinit var viewModelFactory: ViewModelProvider.Factory
//    lateinit var viewModel: ChatViewModel
//
//    private var fotoapparat: Fotoapparat? = null
//    private var isBackCamera = true
//    private var currentFlashMode = 0
//    private val flashModes = arrayOf(
//            Pair(autoFlash(), R.drawable.ic_flash_auto_black_24dp),
//            Pair(on(), R.drawable.ic_flash_on_black_24dp),
//            Pair(off(), R.drawable.ic_flash_off_black_24dp)
//    )
//    private var config = CameraConfiguration(
//            previewFpsRange = highestFps(),
//            flashMode = autoFlash()
//    )
//
//
//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
//                              savedInstanceState: Bundle?): View?
//            = inflater.inflate(R.layout.view_chat_attachments_camera, container, false)
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        val store = findNavController().getViewModelStoreOwner(R.id.chatGraph).viewModelStore
//        viewModel = ViewModelProvider(store, viewModelFactory).get(ChatViewModel::class.java)
//
//        context?.let { ctx ->
//            fotoapparat = Fotoapparat(
//                    context = ctx,
//                    view = cameraView,
//                    scaleType = ScaleType.CenterCrop,
//                    lensPosition = if (isBackCamera) back() else front(),
//                    cameraConfiguration = config
//            )
//        }
//
//        btnFlipCamera.setOnClickListener { switchCamera() }
//        btnFlash.setOnClickListener { switchFlash() }
//        btnStartCameraApp.setOnClickListener { startCameraApp() }
//        btnTakePicture.setOnClickListener { takePicture() }
//    }
//
//    override fun onResume() {
//        super.onResume()
//        fotoapparat?.start()
//    }
//
//    override fun onDestroyView() {
//        super.onDestroyView()
//        fotoapparat?.stop()
//    }
//
//    private fun switchCamera() {
//        isBackCamera = !isBackCamera
//        btnFlipCamera.setImageResource(if (isBackCamera) R.drawable.ic_camera_front_black_24dp else R.drawable.ic_camera_rear_black_24dp)
//        fotoapparat?.switchTo(if (isBackCamera) back() else front(), config)
//    }
//
//    private fun switchFlash() {
//        currentFlashMode = (currentFlashMode + 1) % 3
//        btnFlash.setImageResource(flashModes[currentFlashMode].second)
//        config = config.copy(flashMode = flashModes[currentFlashMode].first)
//        fotoapparat?.updateConfiguration(config)
//    }
//
//    private fun startCameraApp() {
//        context?.let { ctx ->
//            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->
//                intent.resolveActivity(ctx.packageManager)?.also {
//                    startActivityForResult(intent, 1)
//                }
//            }
//        }
//        val intent = Intent("android.media.action.IMAGE_CAPTURE")
//        startActivity(intent)
//    }
//
//    private fun takePicture() {
//        val dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()
//        val fileName = "SotManager_Chat_${DateTime.now().toString("yyyyMMddHHmmss")}.jpg"
//        val file = File(dir, fileName)
//
//        fotoapparat?.takePicture()
//                ?.saveToFile(file)
//                ?.whenAvailable { viewModel.addImageAttachment(file) }
//    }
//
//}