package pl.solventive.localfarmer.main.register

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.joda.time.DateTime
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.network.dto.OAuthToken
import pl.solventive.localfarmer.data.network.dto.uploadDto.RegisterUserDto
import pl.solventive.localfarmer.data.network.repository.LoginRepository
import pl.solventive.localfarmer.data.network.repository.SmsRepository
import pl.solventive.localfarmer.data.network.repository.UserRepository
import pl.solventive.localfarmer.main.LFViewModel
import pl.solventive.localfarmer.util.Event
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
    private val appStateRepo: AppStateRepository,
    private val usersRepo: UserRepository,
    private val smsRepo: SmsRepository,
    private val loginRepo: LoginRepository
) : LFViewModel() {

    private val regex = Regex("[^0-9]")

    val userRegistered =  BehaviorSubject.create<Boolean>()
    val isLoading = MutableLiveData(false)

    val currentStep = MutableLiveData(1)
    val phoneVerificationStatus = MutableLiveData<Event<Boolean>>()
    val codeVerificationStatus = MutableLiveData<Event<Boolean>>()
    val pinSaved = MutableLiveData(false)
    val pinVerificationStatus = MutableLiveData<Event<Boolean>>()
    val nameVerificationStatus = MutableLiveData<Event<Boolean>>()

    val phoneNumber = MutableLiveData("")
    private var verifyingCode = ""
    val inputVeryfingCode = MutableLiveData("")
    var pin: String? = null
    val inputPin = MutableLiveData("")
    val name = MutableLiveData("")

    var resources: Resources? = null

    fun verifyInput() {
        when (currentStep.value) {
            1 -> verifyPhoneNumber()
            2 -> if (verifyingCode == inputVeryfingCode.value) nextStep() else codeVerificationStatus.value = Event(false)
            3 -> {
                if (pinSaved.value == true) {
                    if (inputPin.value == pin) nextStep() else pinVerificationStatus.value = Event(false)
                }
                else if (inputPin.value?.length == 4) savePin()
            }
            4 -> nameVerificationStatus.value = Event((name.value!!.length > 3))
        }
    }

    private fun verifyPhoneNumber() {
        phoneNumber.value?.let {phone ->
            disposable.add(
                usersRepo.verifyPhoneNumber(phone)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ if (!it) sendCodeViaSMS() else { phoneVerificationStatus.value = Event(false)} }, {})
            )
        }
    }

    private fun sendCodeViaSMS() {
        try {
            val uuid = UUID.randomUUID().toString()
            verifyingCode = regex.replace(uuid, "").substring(0,6)
            disposable.add(
                smsRepo.sendSms(phoneNumber.value!!, resources!!.getString(R.string.verification_sms, verifyingCode))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ Timber.tag("SMS API").d(it.errorMsg.plus(it.errorCode).plus(it.messageId)) }, {})
            )
            nextStep()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun nextStep() {
        currentStep.value = currentStep.value!! + 1
    }

    private fun savePin() {
        pin = inputPin.value
        inputPin.value = ""
        pinSaved.value = true
    }

    fun registerUser() {
        isLoading.value = true
        val user = RegisterUserDto(
            phoneNumber.value!!,
            pin!!,
            1,
            name.value!!,
            DateTime.now()
        )
        disposable.add(
            usersRepo.createUser(user)
                .subscribeOn(Schedulers.io())
                .subscribe({ login() }, {})
        )
    }

    private fun login() {
        isLoading.value = true
        val phoneNumber = phoneNumber.value!!
        val password = pin!!

        disposable.add(
            loginRepo.login(phoneNumber, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ saveUserData(it, phoneNumber, password) }, {
                    isLoading.value = false
                    userRegistered.onNext(true)
                })
        )
    }

    private fun saveUserData(token: OAuthToken, phoneNumber: String, password: String) {
        appStateRepo.saveUserData(token, phoneNumber, password)
        disposable.add(
            usersRepo.getUser(appStateRepo.getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent { _, _ -> isLoading.value = false }
                .subscribe({
                    appStateRepo.saveUserName(it.name)
                    userRegistered.onNext(true)
                }, {})
        )

    }

}