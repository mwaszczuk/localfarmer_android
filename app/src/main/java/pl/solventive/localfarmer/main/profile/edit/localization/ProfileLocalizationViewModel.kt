package pl.solventive.localfarmer.main.profile.edit.localization

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ktopencage.OpenCageGeoCoder
import com.ktopencage.ResponseException
import com.ktopencage.model.OpenCageRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.osmdroid.util.GeoPoint
import pl.solventive.localfarmer.main.LFViewModel
import pl.solventive.localfarmer.util.Event
import javax.inject.Inject

class ProfileLocalizationViewModel @Inject constructor(
    private val geoCoder: OpenCageGeoCoder
) : LFViewModel() {

    val geocodeCity = MutableLiveData<Event<String>>()
    val geocodeBuildingNumber = MutableLiveData<Event<String>>()
    val geocodePoint = MutableLiveData<Event<GeoPoint>>()

    var city: String? = null
    var buildingNumber: String? = null
    var point: GeoPoint? = null

    fun forwardGeocode(address: String) {
        val request = OpenCageRequest(address)
        configureRequest(request)
        viewModelScope.launch {
            try {
                val response = handleRequest(request)
                if (!response.results.isNullOrEmpty()) {
                    val result = response.results!![0]
                    result.geometry?.let {
                        val geoPoint = GeoPoint(it.lat, it.lng)
                        point = geoPoint
                        geocodePoint.value = Event(geoPoint)
                    }
                }
            } catch (e: ResponseException) {
                e.printStackTrace()
            }
        }
    }

    fun reverseGeocode(point: GeoPoint) {
        this.point = point
        val request = OpenCageRequest(point.latitude, point.longitude)
        configureRequest(request)
        viewModelScope.launch {
            try {
                val response = handleRequest(request)
                if (!response.results.isNullOrEmpty()) {
                    val result = response.results!![0]
                    result.formatted?.let {
                        city = it
                        geocodeCity.value = Event(it)
                    }
                }
            } catch (e: ResponseException) {
                e.printStackTrace()
            }
        }
    }

    private suspend fun handleRequest(request: OpenCageRequest)
            = withContext(Dispatchers.IO) {
        geoCoder.handleRequest(request)
    }

    private fun configureRequest(request: OpenCageRequest) {
        request.language = "pl"
        request.noDedupe = true
        request.restrictToCountryCode = "pl"
    }

    fun isLocalizationAdded() =
        city != null &&
                point != null
}