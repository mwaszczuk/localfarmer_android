package pl.solventive.localfarmer.main.splash

import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.solventive.localfarmer.BuildConfig
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.main.LFViewModel
import pl.solventive.localfarmer.data.network.dto.OAuthToken
import pl.solventive.localfarmer.data.network.repository.LoginRepository
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val appStateRepo: AppStateRepository,
    loginRepo: LoginRepository
) : LFViewModel() {

    val loginSuccess = MutableLiveData<Boolean>()
    val isHandlerWaiting = MutableLiveData<Boolean>()

    init {
        val phoneNumber = if (appStateRepo.getUserPhone().isNotEmpty()) {
            appStateRepo.getUserPhone()
        } else BuildConfig.LF_DEFAULT_USERNAME
        val password = if (appStateRepo.getUserPassword().isNotEmpty()) {
            appStateRepo.getUserPassword()
        } else BuildConfig.LF_DEFAULT_PASSWORD

        disposable.add(
            loginRepo.login(phoneNumber, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ saveToken(it) }, {})
        )
    }

    private fun saveToken(token: OAuthToken) {
        appStateRepo.saveOAuthToken(token.token)
        appStateRepo.saveRefreshToken(token.refreshToken)
        loginSuccess.value = true
    }
}