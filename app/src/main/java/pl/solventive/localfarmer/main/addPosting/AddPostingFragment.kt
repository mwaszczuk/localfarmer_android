package pl.solventive.localfarmer.main.addPosting

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_add_posting.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.util.components.LFSmallImage
import pl.solventive.localfarmer.util.extension.makeGone
import pl.solventive.localfarmer.util.extension.makeVisible
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class AddPostingFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: AddPostingViewModel

    private val picasso = Picasso.get()
    private val photoButtons: List<LFSmallImage> by lazy { listOf(photo1, photo2, photo3, photo4, photo5, photo6) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val store = findNavController().getViewModelStoreOwner(R.id.addPostingGraph).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(AddPostingViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_add_posting, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disposable.add(
            viewModel.posting.subscribe { showSuccess() }
        )
        bindViews()
        observeChanges()

        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        btnConfirm.setOnClickListener {
            if (viewModel.validate(edtTitle.isNotEmpty(), edtDescription.isNotEmpty())) {
                context?.let { viewModel.addPosting(it) }
            } else showError()
        }
        btnCategory.setOnClickListener { findNavController().navigate(AddPostingFragmentDirections.openCategoryPicker()) }
        btnPrice.setOnClickListener { findNavController().navigate(AddPostingFragmentDirections.openPrice()) }
        btnQuantity.setOnClickListener { findNavController().navigate(AddPostingFragmentDirections.openQuantity()) }
        imgAddPhotos.setOnClickListener { checkPermissions() }

    }

    private fun bindViews() {
        viewModel.selectedCategory?.let {
            btnCategory.setText(viewModel.selectedCategory?.name ?: "")
        }
        viewModel.price?.let {
            btnPrice.setText(resources.getString(R.string.currency_float_per_unit, it, "zł"))
        }
        viewModel.quantity?.let {
            btnQuantity.setText(resources.getString(R.string.quantity, it, viewModel.selectedQuantityUnit?.name ?: ""))
        }
        edtTitle.setText(viewModel.title, true)
        edtDescription.setText(viewModel.description, true)
    }

    private fun observeChanges() {
        viewModel.dbQuantityUnits.observe(viewLifecycleOwner, Observer { viewModel.setProductUnits(it) })
        viewModel.dbProductCategories.observe(viewLifecycleOwner, Observer { viewModel.setCategories(it) })
        edtTitle.observeTextChanges().observe(viewLifecycleOwner, Observer { viewModel.title = it })
        edtDescription.observeTextChanges().observe(viewLifecycleOwner, Observer { viewModel.description = it })
        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { b ->
                if (b) {
                    scrollView.animate().alpha(0.2f).duration = 200
                    carrot.show()
                } else {
                    scrollView.animate().alpha(1f).duration = 200
                    carrot.hide()
                }
            }
        })

        viewModel.photos.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) imgAddPhotos.alpha = 0f
            else imgAddPhotos.alpha = 1f
            for (x in it.indices) {
                photoButtons[x].showImage()
                picasso.load(it[x].file)
                    .resize(512, 512)
                    .centerCrop()
                    .into(photoButtons[x].getImage())
            }
            if (it.size < 6) photoButtons[it.size].hideImage()
            if (it.isEmpty()) photoButtons[0].hideImage()
        })
    }

    private fun checkPermissions() {
        if (checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_EXTERNAL_STORAGE)) { }
            requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                1234
            )
            return
        } else findNavController().navigate(AddPostingFragmentDirections.openPhotoPicker())
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1234) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                findNavController().navigate(AddPostingFragmentDirections.openPhotoPicker())
            }
        }
    }

    private fun showError() {
        Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()
    }

    private fun showSuccess() {
        layoutSuccess.makeVisible()
        btnShowPosting.makeVisible()
        scrollView.animate().alpha(0f).duration = 300
        layoutSuccess.animate().alpha(1f).duration = 300
        btnShowPosting.animate().alpha(1f).duration = 300
        scrollView.makeGone()
        btnShowPosting.setOnClickListener { findNavController().navigateUp() } // this will navigate to the new posting :)
    }
}