package pl.solventive.localfarmer.main.postings.list

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.adapter_home_header.view.*
import kotlinx.android.synthetic.main.adapter_home_posting.view.*
import pl.solventive.localfarmer.BuildConfig
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.network.dto.DictionaryEntryDto
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.util.extension.makeGone
import pl.solventive.localfarmer.util.extension.makeVisible
import pl.solventive.localfarmer.util.extension.toPrettyDateTimeString

class PostingsAdapter(
    private val context: Context,
    private val resources: Resources,
    private val units: BehaviorSubject<List<DictionaryEntryDto>>,
    private val callback: (String) -> Unit,
    private val appStateRepository: AppStateRepository
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var userName: String? = null
    var items = listOf<PostingDto>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 1) HomeHeaderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_home_header, parent, false))
        else PostingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_home_posting, parent, false))
    }

    override fun getItemCount() = items.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is HomeHeaderViewHolder) {
            if (userName != null) {
                holder.itemView.txtUserName.text = resources.getString(R.string.fragment_home_header, userName)
                holder.itemView.txtUserName.makeVisible()
            }
            else holder.itemView.txtUserName.makeGone()
        }
        else {
            val unit = units.value?.firstOrNull { it.id == items[position - 1].quantityUnitId }?.name ?: ""
            holder.itemView.imgProfile.clipToOutline = true
            Glide.with(holder.itemView).load(createGildeUrl(items[position -1].mainPhotoId))
                .into(holder.itemView.imgMainPhoto)
            holder.itemView.txtPrice.text = resources.getString(R.string.currency_float_per_unit, items[position - 1].price, unit)
            holder.itemView.txtTitle.text = items[position - 1].title
//            holder.itemView.txtUser.text = items[position - 1].userId.toString()
            holder.itemView.txtQuantity.text = resources.getString(R.string.quantity, items[position - 1].quantity, unit)
            holder.itemView.txtDate.text = items[position - 1].createdAt.toPrettyDateTimeString(context)
            holder.itemView.cvPosting.setOnClickListener { callback.invoke(items[position - 1].id) }
        }

    }

    fun createGildeUrl(photoId: String) = GlideUrl(
        BuildConfig.API_ADDRESS.plus("v1/files/downloadFile/").plus(photoId),
        LazyHeaders.Builder()
            .addHeader("Authorization", appStateRepository.getOAuthToken())
            .build()
    )

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) 1
        else 2
    }

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view)
    inner class PostingViewHolder(view:View) : RecyclerView.ViewHolder(view)
}