package pl.solventive.localfarmer.main.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_register.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.register.steps.RegisterPhoneNumberFragment
import pl.solventive.localfarmer.main.register.steps.RegisterSetNameFragment
import pl.solventive.localfarmer.main.register.steps.RegisterSetPinFragment
import pl.solventive.localfarmer.main.register.steps.RegisterVerifyNumberFragment
import pl.solventive.localfarmer.util.components.viewPager.LFPagerAdapter
import pl.solventive.localfarmer.util.components.viewPager.LFViewPager
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class RegisterFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: RegisterViewModel

    private val pagerAdapter by lazy { LFPagerAdapter(childFragmentManager) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(RegisterViewModel::class.java)
        viewModel.resources = resources
        viewModel.currentStep.observe(viewLifecycleOwner, Observer { step ->
            updateConfirmButton(step)
            registerViewPager.setCurrentItem(step - 1, true)
        })

        toolbar.setNavigationOnClickListener {
            if (viewModel.currentStep.value != 1) viewModel.currentStep.value = viewModel.currentStep.value!! - 1
            else findNavController().navigateUp()
        }

        disposable.add(
            viewModel.userRegistered.subscribe { if (it) userCreated() }
        )
        viewModel.isLoading.observe(viewLifecycleOwner, Observer { showLoading(it) })

        pagerAdapter.addFragment(RegisterPhoneNumberFragment(viewModel), resources.getString(R.string.fragment_register_tab_phone))
        pagerAdapter.addFragment(RegisterVerifyNumberFragment(viewModel), resources.getString(R.string.fragment_register_tab_verify))
        pagerAdapter.addFragment(RegisterSetPinFragment(viewModel), resources.getString(R.string.fragment_register_tab_pin))
        pagerAdapter.addFragment(RegisterSetNameFragment(viewModel), resources.getString(R.string.fragment_register_tab_name))
        registerViewPager.setScrollDuration(1000)
        registerViewPager.direction = LFViewPager.SwipeDirection.None
        registerViewPager.adapter = pagerAdapter

        btnConfirm.setOnClickListener { viewModel.verifyInput() }
        viewModel.phoneNumber.observe(viewLifecycleOwner, Observer { btnConfirm.isActive = it.length == 9 })
        viewModel.inputVeryfingCode.observe(viewLifecycleOwner, Observer { btnConfirm.isActive = it.length == 6 })
        viewModel.inputPin.observe(viewLifecycleOwner, Observer { btnConfirm.isActive = it.length == 4 })
        viewModel.name.observe(viewLifecycleOwner, Observer { btnConfirm.isActive = it.length >= 3 })
        viewModel.pinSaved.observe(viewLifecycleOwner, Observer { if (it) btnConfirm.changeText(resources.getString(R.string.confirm_po)) })
        viewModel.nameVerificationStatus.observe(viewLifecycleOwner, Observer { if (it.peekContent()) {
            viewModel.registerUser()
            it.getContentIfNotHandled()
        } })

    }

    private fun updateConfirmButton(step: Int) {
        when (step) {
            2 -> btnConfirm.changeText(resources.getString(R.string.next))
            3 -> btnConfirm.changeText(resources.getString(R.string.confirm_za))
            4 -> btnConfirm.changeText(resources.getString(R.string.next))
        }
    }

    private fun showLoading(b: Boolean) {
        if (b) {
            imgLoading.show()
        } else {
            imgLoading.hide()
        }
    }

    private fun userCreated() {
        registerViewPager.animate().alpha(0f).duration = 300
        layoutSuccess.animate().alpha(1f).duration = 300
        toolbar.setNavigationOnClickListener { findNavController().navigate(RegisterFragmentDirections.openHomeFragment()) }
        btnConfirm.setOnClickListener { findNavController().navigate(RegisterFragmentDirections.openHomeFragment()) }
        btnConfirm.changeText(resources.getString(R.string.ok))
    }
}