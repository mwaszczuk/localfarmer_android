package pl.solventive.localfarmer.main.profile

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.fragment_profile_details.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.data.network.dto.UserDto
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.profile.tabs.informations.ProfileInformationsFragment
import pl.solventive.localfarmer.main.profile.tabs.profileOpinions.ProfileOpinionsFragment
import pl.solventive.localfarmer.main.profile.tabs.profileProducts.ProfileProductsFragment
import pl.solventive.localfarmer.util.components.AppBarStateChangeListener
import pl.solventive.localfarmer.util.components.viewPager.LFPagerAdapter
import pl.solventive.localfarmer.util.extension.makeInvisible
import pl.solventive.localfarmer.util.extension.makeVisible
import pl.solventive.localfarmer.util.extension.toGlideUrl
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class ProfileFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: ProfileViewModel

    private val args: ProfileFragmentArgs by navArgs()
    private val adapter: LFPagerAdapter by lazy { LFPagerAdapter(childFragmentManager) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val store = findNavController().getViewModelStoreOwner(R.id.navigation).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disposable.add(
            viewModel.user.subscribe(::updateViews)
        )

        viewModel.init(args.userId)
        if (viewModel.isCurrentUserProfile(args.userId)) {
            btnEditProfile.makeVisible()
            val bundle = Bundle()
            bundle.putInt("userId", args.userId)
            btnEditProfile.setOnClickListener { findNavController().navigate(R.id.openEditProfile, bundle) }
            btnFavourite.makeInvisible()
        }

        observeAppBarCollapse()

        adapter.addFragment(ProfileInformationsFragment(), resources.getString(R.string.fragment_profile_tab_informations))
        adapter.addFragment(ProfileProductsFragment(), resources.getString(R.string.fragment_profile_tab_products))
        adapter.addFragment(ProfileOpinionsFragment(), resources.getString(R.string.fragment_profile_tab_opinions))
        pagerProfileTabs.adapter = adapter
        tabLayout.setupWithViewPager(pagerProfileTabs)
        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        btnNavBackIdle.setOnClickListener { findNavController().navigateUp() }
        btnNavBackCollapsed.setOnClickListener { findNavController().navigateUp() }
    }

    private fun updateViews(user: UserDto) {
        if (user.profilePhotoId != null) {
            Glide.with(this)
                .load(user.profilePhotoId.toGlideUrl(viewModel.getOAuthToken()))
                .centerCrop()
                .apply(RequestOptions.circleCropTransform())
                .into(imgProfilePhoto)
        }
        if (user.backgroundPhotoId != null) {
            Glide.with(this)
                .load(user.backgroundPhotoId.toGlideUrl(viewModel.getOAuthToken()))
                .centerCrop()
                .into(imgBackgroundPhoto)
        }
        txtUserName.text = resources.getString(R.string.name_and_surname, user.name, user.surname)
        txtToolbarHeader.text = resources.getString(R.string.name_and_surname, user.name, user.surname)
        if (user.locationId != null) viewModel.getLocation(user.locationId)
        setOnClickListeners()
    }

    private fun setOnClickListeners() {
        btnFavourite.setOnClickListener {
            btnFavourite.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_border_green_24px))
            viewModel.addToFavourites()
        }
    }

    private fun observeAppBarCollapse() {
        val offsetTopMargin = 80
        collapsingToolbar.scrimVisibleHeightTrigger = convertDptoPx(56f).toInt() + offsetTopMargin
        appBar.addOnOffsetChangedListener(object: AppBarStateChangeListener(offsetTopMargin) {
            override fun onStateChanged(appBarLayout: AppBarLayout?, state: State?) {
                when (state) {
                    State.COLLAPSED -> {
                        txtToolbarHeader.animate().alpha(1f).duration = 250
                        btnNavBackCollapsed.animate().alpha(1f).duration = 250
                        btnNavBackIdle.animate().alpha(0f).duration = 250
                    }
                    State.EXPANDED -> { }
                    State.IDLE -> {
                        txtToolbarHeader.animate().alpha(0f).duration = 250
                        btnNavBackCollapsed.animate().alpha(0f).duration = 250
                        btnNavBackIdle.animate().alpha(1f).duration = 250
                    }
                }
            }
        })
    }

    private fun convertDptoPx(dp: Float) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)


}