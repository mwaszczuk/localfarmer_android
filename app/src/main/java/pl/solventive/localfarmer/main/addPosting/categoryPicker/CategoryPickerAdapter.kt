package pl.solventive.localfarmer.main.addPosting.categoryPicker

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_category_picker.view.*
import pl.solventive.localfarmer.R

class CategoryPickerAdapter(
    private val callback: (CategoryPickerModel, Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items = listOf<CategoryPickerModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
         CategoryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_category_picker, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CategoryViewHolder) {
            holder.itemView.txtName.text = items[position].name
            holder.itemView.boxCategory.isChecked = items[position].isChecked
            holder.itemView.boxCategory.setOnClickListener { callback.invoke(items[position], position) }
        }

    }

    inner class CategoryViewHolder(view: View) : RecyclerView.ViewHolder(view)
}