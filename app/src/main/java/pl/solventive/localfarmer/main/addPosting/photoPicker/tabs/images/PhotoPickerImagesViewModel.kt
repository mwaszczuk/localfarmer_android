package pl.solventive.localfarmer.main.addPosting.photoPicker.tabs.images

import io.reactivex.subjects.BehaviorSubject
import pl.solventive.localfarmer.main.LFViewModel
import java.io.File
import java.util.*
import javax.inject.Inject

class PhotoPickerImagesViewModel @Inject constructor() : LFViewModel() {

    val images = BehaviorSubject.createDefault(emptyList<PhotoPickerImagesModel>())
    var imagesChecked = 0

    fun addImages(files: List<File>, selectedFiles: List<File>) {
        images.onNext(
                files.map { PhotoPickerImagesModel(UUID.randomUUID(), it, selectedFiles.contains(it)) }
                        .sortedByDescending { it.file.lastModified() }
        )
    }

    fun onClicked(item: PhotoPickerImagesModel) {
        if (imagesChecked < 6 || (imagesChecked >= 6 && item.isChecked)) {
            if (item.isChecked) imagesChecked--
            else imagesChecked++
            images.value?.firstOrNull { it.id == item.id }?.isChecked = !item.isChecked
        }
    }

}