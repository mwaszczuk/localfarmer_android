package pl.solventive.localfarmer.main.postings.details

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.preference.PreferenceManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.fragment_posting_details.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.data.network.dto.LocationDto
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.data.network.dto.UserDto
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.util.components.AppBarStateChangeListener
import pl.solventive.localfarmer.util.extension.addMarker
import pl.solventive.localfarmer.util.extension.toGlideUrl
import pl.solventive.localfarmer.util.extension.toPrettyDateTimeString
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class PostingDetailsFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: PostingDetailsViewModel

    private val args: PostingDetailsFragmentArgs by navArgs()

    private val adapter: PhotosAdapter by lazy { PhotosAdapter(viewModel.appStateRepository) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Configuration.getInstance().load(requireContext(), PreferenceManager.getDefaultSharedPreferences(requireContext()))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_posting_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(PostingDetailsViewModel::class.java)
        viewModel.init(args.postingId)

        disposable.addAll(
            viewModel.posting.subscribe(::bindViews),
            viewModel.user.subscribe(::updateUser),
            viewModel.userLocation.subscribe(::showLocation)
        )
        observeLiveData()
        observeAppBarCollapse()

        map.setTileSource(TileSourceFactory.MAPNIK)
        map.zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
        map.setMultiTouchControls(true)
        map.controller.setZoom(6.5)
        map.controller.setCenter(GeoPoint(52.05, 19.08))

        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        btnNavBackIdle.setOnClickListener { findNavController().navigateUp() }
        btnNavBackCollapsed.setOnClickListener { findNavController().navigateUp() }
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    private fun observeLiveData() {
        viewModel.quantityToBuy.observe(viewLifecycleOwner, Observer {quantity ->
            txtBuyQuantity.text = (quantity.getContentIfNotHandled() ?: 0.0).toString()
            viewModel.posting.value?.let {
                txtTotal.text = resources.getString(R.string.currency_float, quantity.peekContent() * it.price)
            }
        })
    }

    private fun bindViews(posting: PostingDto) {
        viewModel.unitDbRepo.getUnitById(posting.quantityUnitId).observe(viewLifecycleOwner, Observer {
            txtQuantity.text = resources.getString(R.string.quantity, posting.quantity, it.name)
            txtPrice.text = resources.getString(R.string.currency_float_per_unit, posting.quantity, it.name)
        })
        adapter.items = posting.photos
        rvPhotos.adapter = adapter
        photosIndicator.setViewPager(rvPhotos)
        rvPhotos.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            true
        }

        setOnClickListeners()
        txtDate.text = posting.createdAt.toPrettyDateTimeString(requireContext())
        txtTitle.text = posting.title
        txtDescription.text = posting.description
    }

    private fun setOnClickListeners() {
        btnPlus.setOnClickListener { viewModel.plusQuantity(1.0) }
        btnMinus.setOnClickListener { viewModel.minusQuantity(1.0) }
        btnFavourite.setOnClickListener {
            btnFavourite.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_border_green_24px))
        }
    }

    private fun updateUser(user: UserDto) {
        btnShowProfile.setOnClickListener { findNavController().navigate(PostingDetailsFragmentDirections.openProfile(user.id)) }
        val rating = user.ratingPoints / user.ratings
        txtUserName.text = resources.getString(R.string.name_and_surname, user.name, user.surname)
        userRating.setValue(rating)
        txtUserRatings.text = resources.getString(R.string.ratingsNumber, user.ratings.toString())
        if (user.profilePhotoId != null)
            Glide.with(this)
                .load(user.profilePhotoId.toGlideUrl(viewModel.getToken()))
                .centerCrop()
                .apply(RequestOptions.circleCropTransform())
                .into(imgProfilePhoto)
    }

    private fun showLocation(location: LocationDto) {
        val point = GeoPoint(location.latitude, location.longitude)
        map.addMarker(requireContext(), point)
        txtLocalization.text = location.city
    }

    private fun observeAppBarCollapse() {
        collapsingToolbar.scrimVisibleHeightTrigger = convertDptoPx(56f).toInt() + 2
        appBar.addOnOffsetChangedListener(object: AppBarStateChangeListener() {
            override fun onStateChanged(appBarLayout: AppBarLayout?, state: State?) {
                when (state) {
                    State.COLLAPSED -> {
                        txtToolbarHeader.animate().alpha(1f).duration = 350
                        btnNavBackCollapsed.animate().alpha(1f).duration = 350
                        btnNavBackIdle.animate().alpha(0f).duration = 350
                    }
                    State.EXPANDED -> { }
                    State.IDLE -> {
                        txtToolbarHeader.animate().alpha(0f).duration = 350
                        btnNavBackCollapsed.animate().alpha(0f).duration = 350
                        btnNavBackIdle.animate().alpha(1f).duration = 350
                    }
                }
            }
        })
    }

    private fun convertDptoPx(dp: Float) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)


}