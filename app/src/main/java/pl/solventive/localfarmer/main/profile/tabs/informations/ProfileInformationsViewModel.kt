package pl.solventive.localfarmer.main.profile.tabs.informations

import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.solventive.localfarmer.BuildConfig
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.network.dto.DictionaryEntryDto
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.data.network.dto.UserDto
import pl.solventive.localfarmer.data.network.repository.PostingRepository
import pl.solventive.localfarmer.data.network.repository.ProductRepository
import pl.solventive.localfarmer.data.network.repository.UserRepository
import pl.solventive.localfarmer.main.LFViewModel
import pl.solventive.localfarmer.util.Event
import javax.inject.Inject

class ProfileInformationsViewModel @Inject constructor(
    val appStateRepository: AppStateRepository,
    private val postingRepo: PostingRepository,
    private val productsRepo: ProductRepository,
    private val usersRepo: UserRepository
) : LFViewModel() {

    val units = BehaviorSubject.create<List<DictionaryEntryDto>>()
    val posting = BehaviorSubject.create<PostingDto>()
    val user = BehaviorSubject.create<UserDto>()

    val quantityToBuy = MutableLiveData(Event(0.0))

    fun init(id: String) {
        disposable.addAll(
            productsRepo.getProductUnits()
                .subscribeOn(Schedulers.io())
                .subscribe(units::onNext) {},
            postingRepo.getPosting(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    posting.onNext(it)
                    getUser(it.userId)
                }, {})
        )
    }

    private fun getUser(id: Int) {
        disposable.add(
            usersRepo.getUser(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user::onNext) {}
        )
    }

    fun createGildeUrl(photoId: String) = GlideUrl(
        BuildConfig.API_ADDRESS.plus("v1/files/downloadFile/").plus(photoId),
        LazyHeaders.Builder()
            .addHeader("Authorization", appStateRepository.getOAuthToken())
            .build()
    )

    fun addToFavourites() {

    }
}