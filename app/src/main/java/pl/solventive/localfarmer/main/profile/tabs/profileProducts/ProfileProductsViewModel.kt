package pl.solventive.localfarmer.main.profile.tabs.profileProducts

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.database.repository.ProductUnitRepository
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.data.network.repository.PostingRepository
import pl.solventive.localfarmer.main.LFViewModel
import javax.inject.Inject

class ProfileProductsViewModel @Inject constructor(
    val appStateRepository: AppStateRepository,
    private val postingRepo: PostingRepository,
    val unitDbRepo: ProductUnitRepository
) : LFViewModel() {

    val postings = BehaviorSubject.create<List<PostingDto>>()
    val isLoading = BehaviorSubject.create<Boolean>()

    fun refresh() {
        disposable.add(
            postingRepo.getPostings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent { _, _ -> isLoading.onNext(false) }
                .subscribe(postings::onNext) {}
        )
    }

    fun getUserName() = appStateRepository.getUserName()

}