package pl.solventive.localfarmer.main.mainContainer.container

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.solventive.localfarmer.data.database.entity.ProductCategory
import pl.solventive.localfarmer.data.database.entity.ProductUnit
import pl.solventive.localfarmer.data.database.repository.ProductCategoryRepository
import pl.solventive.localfarmer.data.database.repository.ProductUnitRepository
import pl.solventive.localfarmer.data.network.repository.ProductRepository
import pl.solventive.localfarmer.main.LFViewModel
import javax.inject.Inject

class HomeContainerViewModel @Inject constructor(
    productRepo: ProductRepository,
    private val unitDbRepo: ProductUnitRepository,
    private val categoryDbRepo: ProductCategoryRepository
): LFViewModel() {

    init {
        disposable.addAll(
            productRepo.getProductUnits()
                .toObservable()
                .flatMapIterable { it.asIterable() }
                .map { unit -> ProductUnit(unit.id, unit.name, unit.apiName) }
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({unitDbRepo.setProductUnits(it)}, {}),
            productRepo.getProductCategories()
                .toObservable()
                .flatMapIterable { it.asIterable() }
                .map { unit -> ProductCategory(unit.id, unit.name, unit.apiName) }
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({categoryDbRepo.setProductCategories(it)}, {})
        )
    }

}