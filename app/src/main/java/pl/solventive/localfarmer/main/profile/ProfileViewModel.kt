package pl.solventive.localfarmer.main.profile

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.network.dto.LocationDto
import pl.solventive.localfarmer.data.network.dto.UserDto
import pl.solventive.localfarmer.data.network.repository.LocationRepository
import pl.solventive.localfarmer.data.network.repository.UserRepository
import pl.solventive.localfarmer.main.LFViewModel
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val appStateRepository: AppStateRepository,
    private val usersRepo: UserRepository,
    private val locationRepo: LocationRepository
) : LFViewModel() {

    val user = BehaviorSubject.create<UserDto>()
    val userLocation = BehaviorSubject.create<LocationDto>()

    fun init(id: Int) {
        disposable.add(
            usersRepo.getUser(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user::onNext) {}
        )
    }

    fun isCurrentUserProfile(userId: Int) = userId == appStateRepository.getUserId()

    fun addToFavourites() {

    }

    fun getOAuthToken() = appStateRepository.getOAuthToken()

    fun getLocation(id: String) {
        disposable.add(
            locationRepo.getLocation(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userLocation::onNext) {}
        )
    }
}