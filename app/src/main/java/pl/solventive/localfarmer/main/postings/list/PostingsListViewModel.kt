package pl.solventive.localfarmer.main.postings.list

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.network.dto.DictionaryEntryDto
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.data.network.repository.PostingRepository
import pl.solventive.localfarmer.data.network.repository.ProductRepository
import pl.solventive.localfarmer.main.LFViewModel
import javax.inject.Inject

class PostingsListViewModel @Inject constructor(
    val appStateRepository: AppStateRepository,
    private val postingRepo: PostingRepository,
    private val productsRepo: ProductRepository
) : LFViewModel() {


    val units = BehaviorSubject.create<List<DictionaryEntryDto>>()
    val postings = BehaviorSubject.create<List<PostingDto>>()
    val isLoading = BehaviorSubject.create<Boolean>()

    init {
        disposable.add(
            productsRepo.getProductUnits()
                .subscribeOn(Schedulers.io())
                .subscribe(units::onNext) {}
        )
    }

    fun refresh() {
        disposable.add(
            postingRepo.getPostings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent { _, _ -> isLoading.onNext(false) }
                .subscribe(postings::onNext) {}
        )
    }

    fun getUserName() = appStateRepository.getUserName()

}