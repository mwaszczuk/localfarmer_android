package pl.solventive.localfarmer.main.postings.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.to_do.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class PostingsListFragment() : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: PostingsListViewModel

//    private val adapter by lazy { PostingsAdapter(requireContext(), resources, viewModel.units, ::onPostingClicked, viewModel.appStateRepository) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.to_do, container, false)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(PostingsListViewModel::class.java)

        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }

//        disposable.addAll(
//            viewModel.postings.subscribe { adapter.items = it
//            adapter.notifyItemRangeChanged(0, adapter.items.size)},
//            viewModel.isLoading.subscribe { if (it) carrot.makeVisible() else carrot.makeGone() }
//        )
//
//        if (viewModel.getUserName().isNotEmpty()) adapter.userName = viewModel.getUserName()
//        rvHome.adapter = adapter
//
//        viewModel.refresh()
    }

//    private fun onPostingClicked(id: String) {
//        findNavController().navigate(HomeContainerFragmentDirections.openPostingDetails(id))
//    }

}
