package pl.solventive.localfarmer.main.mainContainer.container

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.fragment_main_screen.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.mainContainer.drawer.HomeDrawerFragment
import pl.solventive.localfarmer.main.mainContainer.mainScreen.MainScreenFragment
import pl.solventive.localfarmer.util.components.viewPager.HomePagerTransformer
import pl.solventive.localfarmer.util.components.viewPager.LFViewPager
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class HomeContainerFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: HomeContainerViewModel

    val adapter: HomePagerAdapter by lazy { HomePagerAdapter(childFragmentManager) }
    val home: HomeDrawerFragment by lazy { HomeDrawerFragment() }
    val main: MainScreenFragment by lazy { MainScreenFragment(::onNavClicked) }

    var homePager: LFViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(HomeContainerViewModel::class.java)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_main_pager_container, container, false)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewPager()
    }

    private fun onNavClicked() {
        homePager?.setCurrentItem(0, true)
    }

    private fun initViewPager() {
        if(activity != null) {
            homePager = view?.findViewById<LFViewPager>(R.id.homePager)
            val displayMetrics = DisplayMetrics()
            activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
            val width = displayMetrics.widthPixels
            homePager!!.setPageTransformer(false, HomePagerTransformer())
            homePager!!.pageMargin = -(width / 2)
            homePager!!.direction = LFViewPager.SwipeDirection.Left
            homePager!!.offscreenPageLimit = 1
            adapter.addFragment(home, "Home")
            adapter.addFragment(main, "Main")
            homePager!!.adapter = adapter
            homePager!!.currentItem = 1
            homePager!!.setScrollDuration(700)

            homePager!!.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) { }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

                override fun onPageSelected(position: Int) {
                    when (position) {
                        0 -> { homePager!!.direction = LFViewPager.SwipeDirection.Right }
                        else -> { homePager!!.direction = LFViewPager.SwipeDirection.Left }
                    }
                    if (position == 0) {
                        main.mainLayout.catchTouchEvents = true
                        main.mainLayout.setOnClickListener {
                            homePager?.setCurrentItem(
                                1,
                                true
                            )
                        }
                    } else main.mainLayout.catchTouchEvents = false
                }
            })
        }
    }
}