package pl.solventive.localfarmer.main.mainContainer.drawer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_app_drawer.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.mainContainer.container.HomeContainerFragmentDirections
import pl.solventive.localfarmer.util.components.LFDialog
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class HomeDrawerFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: HomeDrawerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_app_drawer, container, false)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(HomeDrawerViewModel::class.java)

        viewModel.loggedOut.observe(viewLifecycleOwner, Observer { findNavController().navigate(HomeContainerFragmentDirections.resetMainScreen()) })

        setupViews(viewModel.isUserLoggedIn())
    }

    private fun setupViews(isLoggedIn: Boolean) {
        setLoggedInButtonsVisibility(isLoggedIn)
        btnLogin.setOnClickListener { findNavController().navigate(HomeContainerFragmentDirections.openLoginFragment()) }
        btnAddPosting.setOnClickListener { findNavController().navigate(HomeContainerFragmentDirections.openAddPostingFragment()) }
        btnMyPostings.setOnClickListener { findNavController().navigate(HomeContainerFragmentDirections.openPostingsList()) }
        btnMyProfile.setOnClickListener { findNavController().navigate(HomeContainerFragmentDirections.openProfileFragment(viewModel.getUserId())) }
        btnMyMessages.setOnClickListener { findNavController().navigate(HomeContainerFragmentDirections.openMessagesFragment()) }
        btnMyFavourites.setOnClickListener { findNavController().navigate(HomeContainerFragmentDirections.openFavouritesFragment()) }
        btnLogout.setOnClickListener { openLogoutDialog() }
        btnSettings.setOnClickListener { findNavController().navigate(HomeContainerFragmentDirections.openSettingsFragment()) }
        btnHelp.setOnClickListener { findNavController().navigate(HomeContainerFragmentDirections.openHelpFragment()) }
        btnInfo.setOnClickListener { findNavController().navigate(HomeContainerFragmentDirections.openInfoFragment()) }
    }

    private fun openLogoutDialog() {
        LFDialog(this, scrollView)
            .title(resources.getString(R.string.fragment_drawer_logout_dialog_title))
            .content(resources.getString(R.string.fragment_drawer_logout_dialog_content))
            .positiveText(resources.getString(R.string.yes))
            .onPositive { viewModel.logout() }
            .negativeText(resources.getString(R.string.back))
            .autoDismiss(true)
            .show()
    }

    private fun setLoggedInButtonsVisibility(visible: Boolean) {
        val visibility = if (visible) View.VISIBLE else View.INVISIBLE
        btnLogin.visibility = if (visible) View.INVISIBLE else View.VISIBLE
        btnAddPosting.visibility = visibility
        btnMyPostings.visibility = visibility
        btnMyProfile.visibility = visibility
        btnMyMessages.visibility = visibility
        btnMyFavourites.visibility = visibility
        btnLogout.visibility = visibility
    }


}