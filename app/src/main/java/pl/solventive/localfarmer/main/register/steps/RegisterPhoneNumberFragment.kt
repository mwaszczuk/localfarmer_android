package pl.solventive.localfarmer.main.register.steps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_register_phone_number.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.register.RegisterViewModel

class RegisterPhoneNumberFragment(
    private val parentViewModel: RegisterViewModel
) : LFFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register_phone_number, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        parentViewModel.phoneVerificationStatus.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { b ->
                if (!b) edtPhoneNumber.showError(resources.getString(R.string.fragment_register_phone_error))
            }
        })

        edtPhoneNumber.observeTextChanges().observe(viewLifecycleOwner, Observer { parentViewModel.phoneNumber.value = it })
    }
}