package pl.solventive.localfarmer.main.mainContainer.mainScreen.home

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.database.repository.ProductUnitRepository
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.data.network.repository.PostingRepository
import pl.solventive.localfarmer.main.LFViewModel
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    val appStateRepository: AppStateRepository,
    val unitDbRepo: ProductUnitRepository,
    private val postingRepo: PostingRepository
) : LFViewModel() {

    val postings = BehaviorSubject.create<List<PostingDto>>()
    val isLoading = BehaviorSubject.create<Boolean>()

    fun refresh() {
        disposable.add(
            postingRepo.getPostings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent { _, _ -> isLoading.onNext(false) }
                .subscribe(postings::onNext) {}
        )
    }

    fun getUserName() = appStateRepository.getUserName()

}