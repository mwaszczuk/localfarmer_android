package pl.solventive.localfarmer.main.login.steps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_login_pin.*
import kotlinx.android.synthetic.main.fragment_register_set_pin.*
import kotlinx.android.synthetic.main.fragment_register_set_pin.numberKeyboard
import kotlinx.android.synthetic.main.fragment_register_set_pin.pin
import kotlinx.android.synthetic.main.fragment_register_set_pin.txtPin
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.login.LoginViewModel

class LoginPinFragment(
    private val parentViewModel: LoginViewModel
) : LFFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login_pin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        parentViewModel.pinVerificationStatus.observe(viewLifecycleOwner, Observer {
            if (!it.peekContent()) {
                txtPin.setTextColor(ContextCompat.getColor(context!!, R.color.red))
                val anim = AnimationUtils.loadAnimation(context, R.anim.shake)
                pin.showError()
                txtPin.startAnimation(anim)
                it.getContentIfNotHandled()
            }
        })
        numberKeyboard.observeKeyboardClicks().observe(viewLifecycleOwner, Observer {
            if (it != -1) pin.addDigit(it)
            else pin.dropLast()
            parentViewModel.inputPin.value = pin.currentPin
        })

    }
}