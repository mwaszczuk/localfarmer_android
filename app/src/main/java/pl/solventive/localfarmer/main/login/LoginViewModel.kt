package pl.solventive.localfarmer.main.login

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.network.dto.OAuthToken
import pl.solventive.localfarmer.data.network.repository.LoginRepository
import pl.solventive.localfarmer.data.network.repository.SmsRepository
import pl.solventive.localfarmer.data.network.repository.UserRepository
import pl.solventive.localfarmer.main.LFViewModel
import pl.solventive.localfarmer.util.Event
import retrofit2.HttpException
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val usersRepo: UserRepository,
    private val loginRepo: LoginRepository,
    private val smsRepo: SmsRepository,
    private val appStateRepo: AppStateRepository
) : LFViewModel() {

    private val regex = Regex("[^0-9]")

    val isLoading = MutableLiveData(false)
    val openRegisterFragment = MutableLiveData<Event<Boolean>>()

    val currentStep = MutableLiveData(1)
    val phoneVerificationStatus = MutableLiveData<Event<Boolean>>()
    val codeVerificationStatus = MutableLiveData<Event<Boolean>>()
    val pinVerificationStatus = MutableLiveData<Event<Boolean>>()


    val phoneNumber = MutableLiveData("")
    private var verifyingCode = ""
    val inputVerifyingCode = MutableLiveData("")
    val inputPin = MutableLiveData("")

    var resources: Resources? = null

    fun verifyInput() {
        when (currentStep.value) {
            1 -> verifyPhoneNumber()
            2 -> if (verifyingCode == inputVerifyingCode.value) nextStep() else codeVerificationStatus.value = Event(false)
            3 -> login()
        }
    }

    private fun verifyPhoneNumber() {
        phoneNumber.value?.let {phone ->
            disposable.add(
                usersRepo.verifyPhoneNumber(phone)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ if (it) sendCodeViaSMS() else { phoneVerificationStatus.value = Event(false)} }, {})
            )
        }
    }

    private fun sendCodeViaSMS() {
        try {
            val uuid = UUID.randomUUID().toString()
            verifyingCode = regex.replace(uuid, "").substring(0,6)
            disposable.add(
                smsRepo.sendSms(phoneNumber.value!!, resources!!.getString(R.string.verification_sms, verifyingCode))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        Timber.tag("SMS API").d(it.errorMsg.plus(it.errorCode).plus(it.messageId)) }, {
                        Timber.tag("SMS API").d(it)
                    })
            )
            nextStep()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun nextStep() {
        currentStep.value = currentStep.value!! + 1
    }

    private fun login() {
        isLoading.value = true
        val phoneNumber = phoneNumber.value ?: ""
        val password = inputPin.value ?: ""

        disposable.add(
            loginRepo.login(phoneNumber, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ saveUserData(it, phoneNumber, password) }, {
                    isLoading.value = false
                    if (it is HttpException) {
                        if (it.code() == 400) {
                            pinVerificationStatus.value = Event(false)
                        }
                    }
                    else it.printStackTrace()
                        })
        )
    }

    private fun saveUserData(token: OAuthToken, phoneNumber: String, password: String) {
        appStateRepo.saveUserData(token, phoneNumber, password)
        disposable.add(
            usersRepo.getUser(appStateRepo.getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent { _, _ -> isLoading.value = false }
                .subscribe({
                    appStateRepo.saveUserName(it.name)
                    pinVerificationStatus.value = Event(true)
                }, {})
        )

    }

}