package pl.solventive.localfarmer.main.mainContainer.drawer

import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.solventive.localfarmer.BuildConfig
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.network.dto.OAuthToken
import pl.solventive.localfarmer.data.network.repository.LoginRepository
import pl.solventive.localfarmer.main.LFViewModel
import pl.solventive.localfarmer.util.Event
import javax.inject.Inject

class HomeDrawerViewModel @Inject constructor(
    private val appStateRepo: AppStateRepository,
    private val loginRepo: LoginRepository
) : LFViewModel() {

    val loggedOut = MutableLiveData<Event<Boolean>>()


    fun logout() {
        appStateRepo.clearUserData()
        disposable.add(
            loginRepo.login(BuildConfig.LF_DEFAULT_USERNAME, BuildConfig.LF_DEFAULT_PASSWORD)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onDefaultTokenReceived(it) }, {})
        )
    }

    fun isUserLoggedIn() = appStateRepo.getUserId() > 0
    fun getUserId() = appStateRepo.getUserId()

    private fun onDefaultTokenReceived(token: OAuthToken) {
        appStateRepo.saveOAuthToken(token.token)
        appStateRepo.saveRefreshToken(token.refreshToken)
        loggedOut.value = Event(true)
    }
}