package pl.solventive.localfarmer.main.postings.details

import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.data.database.repository.ProductCategoryRepository
import pl.solventive.localfarmer.data.database.repository.ProductUnitRepository
import pl.solventive.localfarmer.data.network.dto.LocationDto
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.data.network.dto.UserDto
import pl.solventive.localfarmer.data.network.repository.LocationRepository
import pl.solventive.localfarmer.data.network.repository.PostingRepository
import pl.solventive.localfarmer.data.network.repository.ProductRepository
import pl.solventive.localfarmer.data.network.repository.UserRepository
import pl.solventive.localfarmer.main.LFViewModel
import pl.solventive.localfarmer.util.Event
import javax.inject.Inject

class PostingDetailsViewModel @Inject constructor(
    val unitDbRepo: ProductUnitRepository,
    categoryDbRepo: ProductCategoryRepository,
    val appStateRepository: AppStateRepository,
    private val postingRepo: PostingRepository,
    private val productsRepo: ProductRepository,
    private val usersRepo: UserRepository,
    private val locationRepo: LocationRepository
) : LFViewModel() {

    val posting = BehaviorSubject.create<PostingDto>()
    val user = BehaviorSubject.create<UserDto>()
    val userLocation = BehaviorSubject.create<LocationDto>()

    val quantityToBuy = MutableLiveData(Event(0.0))

    fun init(id: String) {
        disposable.add(
            postingRepo.getPosting(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    posting.onNext(it)
                    getUser(it.userId)
                }, {})
        )
    }

    private fun getUser(id: Int) {
        disposable.add(
            usersRepo.getUser(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.locationId != null) getLocation(it.locationId)
                    user.onNext(it)}) {}
        )
    }

    fun plusQuantity(quantity: Double) {
        quantityToBuy.value?.let {
            val newQuantity = it.peekContent() + quantity
            quantityToBuy.value = Event(newQuantity)
        }
    }

    fun minusQuantity(quantity: Double) {
        quantityToBuy.value?.let {
            val newQuantity = it.peekContent() - quantity
            if (newQuantity >= 0) quantityToBuy.value = Event(newQuantity)
        }
    }

    fun getLocation(id: String) {
        disposable.add(
            locationRepo.getLocation(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userLocation::onNext) {}
        )
    }

    fun getToken() = appStateRepository.getOAuthToken()
}