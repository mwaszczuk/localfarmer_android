package pl.solventive.localfarmer.main.profile.tabs.profileProducts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_home.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.mainContainer.container.HomeContainerFragmentDirections
import pl.solventive.localfarmer.main.mainContainer.mainScreen.home.HomeAdapter
import pl.solventive.localfarmer.util.extension.makeGone
import pl.solventive.localfarmer.util.extension.makeVisible
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class ProfileProductsFragment() : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: ProfileProductsViewModel

    private val adapter by lazy { HomeAdapter(requireContext(), resources, viewModel.unitDbRepo, ::onPostingClicked, viewModel.appStateRepository) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ProfileProductsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_home, container, false)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disposable.addAll(
            viewModel.postings.subscribe { adapter.items = it
            adapter.notifyItemRangeChanged(0, adapter.items.size)},
            viewModel.isLoading.subscribe { if (it) carrot.makeVisible() else carrot.makeGone() }
        )

        if (viewModel.getUserName().isNotEmpty()) adapter.userName = viewModel.getUserName()
        rvHome.adapter = adapter

        viewModel.refresh()
    }

    private fun onPostingClicked(id: String) {
        findNavController().navigate(HomeContainerFragmentDirections.openPostingDetails(id))
    }

}
