package pl.solventive.localfarmer.main.profile.edit.localization

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.fragment_edit_profile_localization.*
import org.osmdroid.config.Configuration
import org.osmdroid.events.MapEventsReceiver
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.overlay.MapEventsOverlay
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.profile.edit.EditProfileViewModel
import pl.solventive.localfarmer.util.extension.addMarker
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class ProfileLocalizationFragment: LFFragment(), Injectable, MapEventsReceiver {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var editProfileViewModel: EditProfileViewModel
    lateinit var viewModel: ProfileLocalizationViewModel

    private val eventOverlay = MapEventsOverlay( this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Configuration.getInstance().load(requireContext(), PreferenceManager.getDefaultSharedPreferences(requireContext()))

        val store = findNavController().getViewModelStoreOwner(R.id.editProfileGraph).viewModelStore
        editProfileViewModel = ViewModelProvider(store, viewModelFactory).get(EditProfileViewModel::class.java)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ProfileLocalizationViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_profile_localization, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editProfileViewModel.city?.let { edtLocalization.setText(it) }
        editProfileViewModel.point?.let { addMarker(it) }
        observeLiveDatas()

        map.setTileSource(TileSourceFactory.MAPNIK)
        map.zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
        map.setMultiTouchControls(true)
        map.controller.setZoom(6.5)
        map.controller.setCenter(GeoPoint(52.05, 19.08))

        map.overlays.add(eventOverlay)

        edtLocalization.observeTextChanges().observe(viewLifecycleOwner, Observer {
            viewModel.city = it
            viewModel.forwardGeocode(it)
        })
        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        btnConfirm.setOnClickListener { saveAndNavigateBack() }
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun longPressHelper(p: GeoPoint?): Boolean {
        addMarker(p)
        p?.let { viewModel.reverseGeocode(it)}
        return true
    }

    override fun singleTapConfirmedHelper(p: GeoPoint?): Boolean {
        return false
    }

    private fun observeLiveDatas() {
        viewModel.geocodeCity.observe(viewLifecycleOwner, Observer { if (!it.hasBeenHandled) edtLocalization.setText(it.getContentIfNotHandled() ?: "") })
        viewModel.geocodeBuildingNumber.observe(viewLifecycleOwner, Observer {  })
        viewModel.geocodePoint.observe(viewLifecycleOwner, Observer { addMarker(it.getContentIfNotHandled()) })
    }

    private fun addMarker(point: GeoPoint?){
        map.overlays.clear()
        map.overlays.add(eventOverlay)
        map.addMarker(requireContext(), point)
    }

    private fun saveAndNavigateBack() {
        if(viewModel.isLocalizationAdded()) {
            editProfileViewModel.city = viewModel.city
            editProfileViewModel.point = viewModel.point
        }
        findNavController().navigateUp()
    }
}