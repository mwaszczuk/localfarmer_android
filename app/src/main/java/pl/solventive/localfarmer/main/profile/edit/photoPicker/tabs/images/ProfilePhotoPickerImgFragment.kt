package pl.solventive.localfarmer.main.profile.edit.photoPicker.tabs.images

import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.fragment_photo_picker_photos.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.profile.edit.EditProfileViewModel
import pl.solventive.localfarmer.util.components.imageCropper.ImageCropperFragment
import pl.solventive.localfarmer.util.injection.Injectable
import java.io.File
import javax.inject.Inject

class ProfilePhotoPickerImgFragment(
    val imageType: Int
) : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: ProfilePhotoPickerImgViewModel
    lateinit var editProfileViewModel: EditProfileViewModel

    private val adapter = ProfilePhotoPickerImgAdapter(::onItemClicked)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_photo_picker_photos, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val store = findNavController().getViewModelStoreOwner(R.id.navigation).viewModelStore
        viewModel = ViewModelProvider(this, viewModelFactory).get(ProfilePhotoPickerImgViewModel::class.java)
        editProfileViewModel = ViewModelProvider(store, viewModelFactory).get(EditProfileViewModel::class.java)
        disposable.add(
            viewModel.images.subscribe {
                adapter.items = it
            }
        )

        rvImages.layoutManager = GridLayoutManager(view.context, 3)
        (rvImages.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        rvImages.adapter = adapter

        val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(MediaStore.MediaColumns.DATA)

        activity?.contentResolver?.query(uri, projection, null, null, null)?.use { cursor ->
            val imagePaths = mutableListOf<String>()
            while (cursor.moveToNext()) {
                val absolutePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
                imagePaths.add(absolutePath)
            }
//            val selectedAttachments = editProfileViewModel.photos.value!!.map { it.file }
            viewModel.addImages(imagePaths.map { File(it) }, emptyList())
        }
    }

    private fun onItemClicked(item: ProfilePhotoPickerImgModel) {
        val bundle = Bundle()
        bundle.putParcelable(ImageCropperFragment.IMAGE_URI, item.file.toUri())
        bundle.putInt(ImageCropperFragment.IMAGE_TYPE, imageType)
        findNavController().navigate(R.id.openImageCropper, bundle)
//        if (item.isChecked) {
//            editProfileViewModel.removePhoto(item)
//        } else {
//            if (viewModel.imagesChecked < 6) editProfileViewModel.addPhoto(item)
//        }
//        viewModel.onClicked(item)
    }
}