package pl.solventive.localfarmer.main.mainContainer.mainScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_main_screen.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.mainContainer.container.HomeContainerFragmentDirections
import pl.solventive.localfarmer.main.mainContainer.mainScreen.home.HomeFragment
import pl.solventive.localfarmer.util.components.LFDialog
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class MainScreenFragment(
    private val navClickCallback: () -> Unit
) : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: MainScreenViewModel

    var isDrawerVisible = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_main_screen, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MainScreenViewModel::class.java)
        if (childFragmentManager.findFragmentByTag("homeFragment") == null){
            childFragmentManager.beginTransaction().replace(R.id.mainScreenInnerWindow, HomeFragment(), "homeFragment")
                .commit()
        }

        bind()
    }

    private fun bind() {
        toolbar.setNavigationOnClickListener { navClickCallback.invoke() }
        btnAddPosting.setOnClickListener {
            if (!viewModel.isUserLoggedIn()) showLoginDialog()
            else openAddPosting()
        }
    }

    private fun showLoginDialog() {
        LFDialog(this, homeCard)
            .title(resources.getString(R.string.fragment_home_login_dialog_title))
            .content(resources.getString(R.string.fragment_home_login_dialog_content))
            .positiveText(resources.getString(R.string.fragment_home_login_dialog_login))
            .onPositive { openLoginFragment() }
            .negativeText(resources.getString(R.string.back))
            .autoDismiss(true)
            .show()
    }

    private fun openAddPosting() {
        findNavController().navigate(HomeContainerFragmentDirections.openAddPostingFragment())
    }

    private fun openLoginFragment() {
        findNavController().navigate(HomeContainerFragmentDirections.openLoginFragment())
    }

}
