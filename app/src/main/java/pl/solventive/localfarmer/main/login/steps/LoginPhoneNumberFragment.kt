package pl.solventive.localfarmer.main.login.steps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_login_phone_number.*
import kotlinx.android.synthetic.main.fragment_register_phone_number.edtPhoneNumber
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.login.LoginViewModel
import pl.solventive.localfarmer.util.Event

class LoginPhoneNumberFragment(
    private val parentViewModel: LoginViewModel
) : LFFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login_phone_number, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        parentViewModel.phoneVerificationStatus.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { b ->
                if (!b) edtPhoneNumber.showError(resources.getString(R.string.fragment_login_phone_error))
            }
        })

        txtCreateAccount.setOnClickListener {
            parentViewModel.openRegisterFragment.value = Event(true)
        }
        edtPhoneNumber.observeTextChanges()
            .observe(viewLifecycleOwner, Observer { parentViewModel.phoneNumber.value = it })
    }
}