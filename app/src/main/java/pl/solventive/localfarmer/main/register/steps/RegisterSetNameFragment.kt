package pl.solventive.localfarmer.main.register.steps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_register_name.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.register.RegisterViewModel

class RegisterSetNameFragment(
    private val parentViewModel: RegisterViewModel
) : LFFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register_name, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        parentViewModel.nameVerificationStatus.observe(viewLifecycleOwner, Observer {
            if (!it.peekContent()) {
                edtName.showError(resources.getString(R.string.fragment_register_set_name_error))
            }
        })

        edtName.observeTextChanges().observe(viewLifecycleOwner, Observer { parentViewModel.name.value = it })
    }
}