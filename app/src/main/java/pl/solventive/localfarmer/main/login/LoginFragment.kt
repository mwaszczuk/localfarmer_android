package pl.solventive.localfarmer.main.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_register.btnConfirm
import kotlinx.android.synthetic.main.fragment_register.imgLoading
import kotlinx.android.synthetic.main.fragment_register.toolbar
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.login.steps.LoginPhoneNumberFragment
import pl.solventive.localfarmer.main.login.steps.LoginPinFragment
import pl.solventive.localfarmer.main.login.steps.LoginVerifyNumberFragment
import pl.solventive.localfarmer.util.Event
import pl.solventive.localfarmer.util.components.viewPager.LFPagerAdapter
import pl.solventive.localfarmer.util.components.viewPager.LFViewPager
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class LoginFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: LoginViewModel

    private val pagerAdapter by lazy { LFPagerAdapter(childFragmentManager) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? 
        = inflater.inflate(R.layout.fragment_login, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
        viewModel.resources = resources
        viewModel.currentStep.observe(viewLifecycleOwner, Observer { step ->
            updateConfirmButton(step)
            loginViewPager.setCurrentItem(step - 1, true)
        })

        toolbar.setNavigationOnClickListener {
            if (viewModel.currentStep.value != 1) viewModel.currentStep.value = viewModel.currentStep.value!! - 1
            else findNavController().navigateUp()
        }

        viewModel.isLoading.observe(viewLifecycleOwner, Observer { showLoading(it) })

        pagerAdapter.addFragment(LoginPhoneNumberFragment(viewModel), resources.getString(R.string.fragment_login_tab_phone))
        pagerAdapter.addFragment(LoginVerifyNumberFragment(viewModel), resources.getString(R.string.fragment_login_tab_verify))
        pagerAdapter.addFragment(LoginPinFragment(viewModel), resources.getString(R.string.fragment_login_tab_pin))
        loginViewPager.setScrollDuration(1000)
        loginViewPager.direction = LFViewPager.SwipeDirection.None
        loginViewPager.adapter = pagerAdapter

        btnConfirm.setOnClickListener { viewModel.verifyInput() }
        viewModel.openRegisterFragment.observe(viewLifecycleOwner, Observer { openRegisterFragment(it) })
        viewModel.phoneNumber.observe(viewLifecycleOwner, Observer { btnConfirm.isActive = it.length == 9 })
        viewModel.inputVerifyingCode.observe(viewLifecycleOwner, Observer { btnConfirm.isActive = it.length == 6 })
        viewModel.inputPin.observe(viewLifecycleOwner, Observer { btnConfirm.isActive = it.length == 4 })
        viewModel.pinVerificationStatus.observe(viewLifecycleOwner, Observer {
            if (it.peekContent()) {
                it.getContentIfNotHandled()
                loginSuccesful()
        }})
    }

    private fun updateConfirmButton(step: Int) {
        when (step) {
            2 -> btnConfirm.changeText(resources.getString(R.string.next))
            3 -> btnConfirm.changeText(resources.getString(R.string.fragment_login_login))
        }
    }

    private fun showLoading(b: Boolean) {
        if (b) {
            imgLoading.show()
        } else {
            imgLoading.hide()
        }
    }

    private fun loginSuccesful() {
        loginViewPager.animate().alpha(0f).duration = 300
        layoutSuccess.animate().alpha(1f).duration = 300
        toolbar.setNavigationOnClickListener { findNavController().navigate(LoginFragmentDirections.openHomeFragment()) }
        btnConfirm.setOnClickListener { findNavController().navigate(LoginFragmentDirections.openHomeFragment()) }
        btnConfirm.changeText(resources.getString(R.string.ok))
    }

    private fun openRegisterFragment(event: Event<Boolean>) {
        event.getContentIfNotHandled()?.let {
            if (it) findNavController().navigate(LoginFragmentDirections.openRegisterFragment())
        }
    }
}