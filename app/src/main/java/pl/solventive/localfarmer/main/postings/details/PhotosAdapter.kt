package pl.solventive.localfarmer.main.postings.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.adapter_photo.view.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.data.cache.AppStateRepository
import pl.solventive.localfarmer.util.extension.toGlideUrl

class PhotosAdapter(
    private val appStateRepository: AppStateRepository
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
         PhotoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_photo, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Glide.with(holder.itemView)
            .load((items[position].toGlideUrl(appStateRepository.getOAuthToken())))
            .centerCrop()
            .into(holder.itemView.imgPhoto)
    }

    inner class PhotoViewHolder(view: View) : RecyclerView.ViewHolder(view)
}