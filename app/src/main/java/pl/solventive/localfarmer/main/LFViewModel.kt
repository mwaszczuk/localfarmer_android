package pl.solventive.localfarmer.main

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class LFViewModel : ViewModel() {

    protected val disposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}