package pl.solventive.localfarmer.main.addPosting.photoPicker.tabs.images

import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.fragment_photo_picker_photos.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.addPosting.AddPostingViewModel
import pl.solventive.localfarmer.util.injection.Injectable
import java.io.File
import javax.inject.Inject

class PhotoPickerImagesFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: PhotoPickerImagesViewModel
    lateinit var addPostingViewModel: AddPostingViewModel

    private val adapter = PhotoPickerImagesAdapter(::onItemClicked)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val store = findNavController().getViewModelStoreOwner(R.id.addPostingGraph).viewModelStore
        viewModel = ViewModelProvider(this, viewModelFactory).get(PhotoPickerImagesViewModel::class.java)
        addPostingViewModel = ViewModelProvider(store, viewModelFactory).get(AddPostingViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_photo_picker_photos, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disposable.add(
            viewModel.images.subscribe { adapter.items = it}
        )
        viewModel.imagesChecked = addPostingViewModel.photos.value?.size ?: 0

        rvImages.layoutManager = GridLayoutManager(view.context, 3)
        (rvImages.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        rvImages.adapter = adapter

        loadPhotos()
    }

    private fun loadPhotos() {
        val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(MediaStore.MediaColumns.DATA)
        activity?.contentResolver?.query(uri, projection, null, null, null)?.use { cursor ->
            val imagePaths = mutableListOf<String>()
            while (cursor.moveToNext()) {
                val absolutePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
                imagePaths.add(absolutePath)
            }
            val selectedAttachments = addPostingViewModel.photos.value!!.map { it.file }
            viewModel.addImages(imagePaths.map { File(it) }, selectedAttachments)
        }
    }

    private fun onItemClicked(item: PhotoPickerImagesModel) {
        if (item.isChecked) {
            addPostingViewModel.removePhoto(item)
        } else {
            if (viewModel.imagesChecked < 6) addPostingViewModel.addPhoto(item)
        }
        viewModel.onClicked(item)
    }

}