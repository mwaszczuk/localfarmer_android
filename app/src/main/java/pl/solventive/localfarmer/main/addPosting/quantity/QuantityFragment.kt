package pl.solventive.localfarmer.main.addPosting.quantity

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_category_picker.toolbar
import kotlinx.android.synthetic.main.fragment_product_quantity.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.addPosting.AddPostingViewModel
import pl.solventive.localfarmer.main.addPosting.UnitsAdapter
import pl.solventive.localfarmer.main.addPosting.categoryPicker.CategoryPickerModel
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class QuantityFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: AddPostingViewModel

    private val adapter by lazy { UnitsAdapter(::onItemClicked) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val store = findNavController().getViewModelStoreOwner(R.id.addPostingGraph).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(AddPostingViewModel::class.java)
        adapter.items = viewModel.quantityUnits
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            =inflater.inflate(R.layout.fragment_product_quantity, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        rvUnits.adapter = adapter
        if (viewModel.quantity != null) edtQuantity.setText("%.2f".format(viewModel.quantity).replace(",", "."))
        txtUnit.text = viewModel.selectedQuantityUnit?.name

        edtQuantity.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.quantity = s.toString().toDoubleOrNull()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        if (edtQuantity.requestFocus()) {
            (edtQuantity.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).toggleSoftInput(0, 0)
        }
    }

    private fun onItemClicked(unit: CategoryPickerModel, position: Int) {
        txtUnit.text = unit.name
        if (!viewModel.quantityUnits[position].isChecked) {
            if (viewModel.selectedQuantityUnit != null) viewModel.quantityUnits[viewModel.selectedQuantityUnit!!.id - 1].isChecked = false
            viewModel.quantityUnits[position].isChecked = true
            viewModel.selectedQuantityUnit = unit
        }
        adapter.items = viewModel.quantityUnits
        adapter.notifyItemRangeChanged(0, adapter.itemCount)
    }

}