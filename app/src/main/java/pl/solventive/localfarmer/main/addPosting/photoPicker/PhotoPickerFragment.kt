package pl.solventive.localfarmer.main.addPosting.photoPicker

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_photo_picker.*
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.addPosting.AddPostingViewModel
import pl.solventive.localfarmer.main.addPosting.photoPicker.tabs.images.PhotoPickerImagesFragment
import javax.inject.Inject


class PhotoPickerFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: AddPostingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val store = findNavController().getViewModelStoreOwner(R.id.navigation).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(AddPostingViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_photo_picker, container, false)

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        btnReady.setOnClickListener { findNavController().navigateUp() }

        val pagerAdapter = AttachmentsPagerAdapter(childFragmentManager)
        pagerAdapter.addFragment(PhotoPickerImagesFragment())
        viewPager.adapter = pagerAdapter
        viewPager.offscreenPageLimit = 2
    }

    private inner class AttachmentsPagerAdapter(manager: FragmentManager)
        : FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private val fragments = ArrayList<Fragment>()

        override fun getPageTitle(position: Int) = ""
        override fun getItem(position: Int) = fragments[position]
        override fun getCount() = fragments.size

        fun addFragment(fragment: Fragment) {
            fragments.add(fragment)
        }

    }

}