package pl.solventive.localfarmer.main

import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import pl.solventive.localfarmer.LocalFarmerApplication
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.main.profile.edit.EditProfileFragment
import pl.solventive.localfarmer.util.components.ConnectionLiveData
import pl.solventive.localfarmer.util.components.imageCropper.ImageCropperCallback
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasAndroidInjector, ImageCropperCallback{

    private lateinit var appBarConfiguration: AppBarConfiguration

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    private val noNetworkSnackBar by lazy {
        val snackBar = Snackbar.make(
            findViewById(R.id.main_activity_layout),
            R.string.activity_main_snackbar_no_internet_connection,
            Snackbar.LENGTH_INDEFINITE
        )
        snackBar.setAction(R.string.ok) { snackBar.dismiss() }
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val connectionLiveData = ConnectionLiveData(application as LocalFarmerApplication)
        connectionLiveData.observe(this, Observer { if (!it) noNetworkSnackBar.show() else noNetworkSnackBar.dismiss() })

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }

        setContentView(R.layout.activity_main)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onImageCropped(image: Bitmap, imageType: Int) {
        // Android navigation component is not fun at all ;x
        Handler().postDelayed( {
            (supportFragmentManager.primaryNavigationFragment?.childFragmentManager?.primaryNavigationFragment as EditProfileFragment).onImageCropped(image, imageType)
        }, 200)
    }
}
