package pl.solventive.localfarmer.main.addPosting

import android.content.Context
import android.content.SharedPreferences
import android.webkit.MimeTypeMap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.zelory.compressor.Compressor
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.apache.commons.io.FileUtils
import org.joda.time.DateTime
import pl.solventive.localfarmer.data.database.entity.ProductCategory
import pl.solventive.localfarmer.data.database.entity.ProductUnit
import pl.solventive.localfarmer.data.database.repository.ProductCategoryRepository
import pl.solventive.localfarmer.data.database.repository.ProductUnitRepository
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadPostingDto
import pl.solventive.localfarmer.data.network.repository.FileRepository
import pl.solventive.localfarmer.data.network.repository.PostingRepository
import pl.solventive.localfarmer.data.network.repository.ProductRepository
import pl.solventive.localfarmer.main.LFViewModel
import pl.solventive.localfarmer.main.addPosting.categoryPicker.CategoryPickerModel
import pl.solventive.localfarmer.main.addPosting.photoPicker.tabs.images.PhotoPickerImagesModel
import pl.solventive.localfarmer.util.Event
import pl.solventive.localfarmer.util.extension.getUserId
import java.io.File
import javax.inject.Inject

class AddPostingViewModel @Inject constructor(
    private val prefs: SharedPreferences,
    unitDbRepo: ProductUnitRepository,
    categoryDbRepo: ProductCategoryRepository,
    private val productRepo: ProductRepository,
    private val fileRepo: FileRepository,
    private val postingRepo: PostingRepository
) : LFViewModel() {

    val isLoading = MutableLiveData<Event<Boolean>>()
    val posting = BehaviorSubject.create<PostingDto>()
    var dbQuantityUnits = unitDbRepo.getProductUnits()
    var dbProductCategories = categoryDbRepo.getProductCategories()

    val photos = MutableLiveData<List<PhotoPickerImagesModel>>(emptyList())
    var productCategories = listOf<CategoryPickerModel>()
    var selectedCategory: CategoryPickerModel? = null
    var quantityUnits = listOf<CategoryPickerModel>()
    var quantity: Double? = null
    var price: Double? = null
    var selectedQuantityUnit: CategoryPickerModel? = null
    var title = ""
    var description = ""

    fun setProductUnits(units: List<ProductUnit>) {
        if (quantityUnits.isEmpty()) {
            quantityUnits = units.map { CategoryPickerModel(it) }
            selectedQuantityUnit = quantityUnits[0]
            quantityUnits[0].isChecked = true
        }
    }

    fun setCategories(categories: List<ProductCategory>) {
        if (productCategories.isEmpty()) productCategories = categories.map { CategoryPickerModel(it) }
    }

    fun removePhoto(photo: PhotoPickerImagesModel) {
        val mPhotos = arrayListOf<PhotoPickerImagesModel>()
        mPhotos.addAll(photos.value!!)
        mPhotos.removeAll { it.file.name == photo.file.name }
        photos.value = mPhotos
    }

    fun addPhoto(photo: PhotoPickerImagesModel) {
        val mPhotos = arrayListOf<PhotoPickerImagesModel>()
        mPhotos.addAll(photos.value!!)
        mPhotos.add(photo)
        photos.value = mPhotos
    }

    fun addPosting(context: Context) {
        isLoading.value = Event(true)
        viewModelScope.launch {
            val compressedFiles = compressPhotos(context)
            if (compressedFiles != null) postPhotos(compressedFiles)
        }

    }

    private suspend fun compressPhotos(context: Context): List<File>?
        = withContext(Dispatchers.IO) {
        val files = mutableListOf<File>()
            photos.value?.forEach {
                files.add(Compressor.compress(context, it.file))
            }
         files
        }


    private  fun postPhotos(photos: List<File>) {
        val files = mutableListOf<MultipartBody.Part>()
        photos.forEach {
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(FileUtils.getExtension(it.name)) ?: return
            val requestFile = it.asRequestBody(mimeType.toMediaTypeOrNull())
            files.add(MultipartBody.Part.createFormData("file", it.name, requestFile))
        }
        disposable.add(
            Observable.fromIterable(files)
                .flatMap { file ->
                    fileRepo.postFile(file)
                        .map { it.id }
                        .toObservable()
                }
                .toList()
                .subscribeOn(Schedulers.io())
                .subscribe({ uploadPosting(it) } , {})

        )
    }

    private fun uploadPosting(filesIds: List<String>) {
        val posting = createPosting(filesIds)
        disposable.add(
            postingRepo.addPosting(posting)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent { _, _ -> isLoading.value = Event(false) }
                .subscribe({ this.posting.onNext(it) }, {})
        )
    }

    private fun createPosting(filesIds: List<String>) =
        UploadPostingDto(
            title = title,
            description = description,
            userId = prefs.getUserId(),
            quantityUnitId = selectedQuantityUnit!!.id,
            categoryId = selectedCategory!!.id,
            quantity = quantity ?: 0.0,
            price = price ?: 0.0,
            tags = emptyList(),
            mainPhotoId = filesIds[0],
            photos = filesIds,
            latitude = null,
            longitude = null,
            createdAt = DateTime.now(),
            expiryDate = DateTime.now().plusDays(7),
            status = 1
        )

    fun validate(titleOk: Boolean, descriptionOk: Boolean) =
        !photos.value.isNullOrEmpty() &&
                quantity != null &&
                price != null &&
                selectedCategory != null &&
                titleOk &&
                descriptionOk
}