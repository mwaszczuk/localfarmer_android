package pl.solventive.localfarmer.main.profile.tabs.informations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.fragment_profile_informations.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import pl.solventive.localfarmer.R
import pl.solventive.localfarmer.data.network.dto.LocationDto
import pl.solventive.localfarmer.data.network.dto.UserDto
import pl.solventive.localfarmer.main.LFFragment
import pl.solventive.localfarmer.main.profile.ProfileViewModel
import pl.solventive.localfarmer.util.extension.addMarker
import pl.solventive.localfarmer.util.injection.Injectable
import javax.inject.Inject

class ProfileInformationsFragment : LFFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Configuration.getInstance().load(requireContext(), PreferenceManager.getDefaultSharedPreferences(requireContext()))

        val store = findNavController().getViewModelStoreOwner(R.id.navigation).viewModelStore
        viewModel = ViewModelProvider(store, viewModelFactory).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_informations, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disposable.addAll(
            viewModel.user.subscribe(::updateViews),
            viewModel.userLocation.subscribe(::showLocation)
        )

        map.setTileSource(TileSourceFactory.MAPNIK)
        map.zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
        map.setMultiTouchControls(true)
        map.controller.setZoom(6.5)
        map.controller.setCenter(GeoPoint(52.05, 19.08))

        observeLiveData()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    private fun observeLiveData() {

    }

    private fun updateViews(user: UserDto) {
        setOnClickListeners()
        btnPhone.text = resources.getString(R.string.fragment_profile_phone, user.phoneNumber)
        txtDescription.text = if (!user.description.isNullOrEmpty()) user.description else resources.getString(R.string.fragment_profile_no_description)
    }

    private fun setOnClickListeners() {

    }

    private fun showLocation(location: LocationDto) {
        val point = GeoPoint(location.latitude, location.longitude)
        map.addMarker(requireContext(), point)
        txtLocalization.text = location.city
    }

}