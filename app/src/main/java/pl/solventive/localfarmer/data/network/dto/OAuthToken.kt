package pl.solventive.localfarmer.data.network.dto

import com.google.gson.annotations.SerializedName

data class OAuthToken(
    @SerializedName("access_token")
    val token: String,
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("refresh_token")
    val refreshToken: String,
    @SerializedName("expires_in")
    val expiresIn: Int,
    @SerializedName("scope")
    val scope: String,
    @SerializedName("jti")
    val jti: String
)