package pl.solventive.localfarmer.data.network.repository

import android.content.SharedPreferences
import okhttp3.MultipartBody
import pl.solventive.localfarmer.data.network.api.FilesApi
import pl.solventive.localfarmer.util.extension.getOAuthToken
import javax.inject.Inject

class FileRepository @Inject constructor(
    private val prefs: SharedPreferences,
    private val api: FilesApi
) {

    fun downloadFile(fileId: String) = api.downloadFile(prefs.getOAuthToken(),  fileId)

    fun postFile(multipart: MultipartBody.Part) = api.postFile(prefs.getOAuthToken(), multipart)

    fun postMultipleFiles(
        multiparts: List<MultipartBody.Part>
    ) = api.postMultipleFiles(prefs.getOAuthToken(), multiparts)
}