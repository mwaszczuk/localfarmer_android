package pl.solventive.localfarmer.data.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update
import pl.solventive.localfarmer.data.database.entity.ProductUnit

@Dao
interface ProductUnitDao {

    @Query("SELECT * FROM product_unit WHERE id = :unitId LIMIT 1")
    fun selectUnitById(unitId: Int): LiveData<ProductUnit>

    @Query("SELECT * FROM product_unit")
    fun selectAllUnits(): LiveData<List<ProductUnit>>

    @Update
    fun update(unit: ProductUnit)

    @Insert(onConflict = REPLACE)
    fun insert(unit: ProductUnit)

    @Insert(onConflict = REPLACE)
    fun insert(units: List<ProductUnit>)

}