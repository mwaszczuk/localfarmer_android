package pl.solventive.localfarmer.data.network.api

import io.reactivex.Single
import pl.solventive.localfarmer.data.network.dto.DictionaryEntryDto
import retrofit2.http.GET
import retrofit2.http.Header

interface ProductsApi {

    @GET("v1/products/categories")
    fun getProductCategories(
        @Header("Authorization") token: String
    ) : Single<List<DictionaryEntryDto>>

    @GET("v1/products/units")
    fun getProductUnits(
        @Header("Authorization") token: String
    ) : Single<List<DictionaryEntryDto>>

}