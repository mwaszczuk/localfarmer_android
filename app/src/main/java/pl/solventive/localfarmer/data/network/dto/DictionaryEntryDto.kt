package pl.solventive.localfarmer.data.network.dto

import com.google.gson.annotations.SerializedName

data class DictionaryEntryDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("apiName")
    val apiName: String
)