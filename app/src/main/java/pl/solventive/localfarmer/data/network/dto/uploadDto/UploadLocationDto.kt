package pl.solventive.localfarmer.data.network.dto.uploadDto

data class UploadLocationDto(
    val name: String,
    val userId: Int,
    val city: String,
    val street: String? = null,
    val postalCode: String? = null,
    val buildingNumber: String? = null,
    val latitude: Double,
    val longitude: Double

)