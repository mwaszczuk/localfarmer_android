package pl.solventive.localfarmer.data.network.dto.uploadDto

import com.google.gson.annotations.SerializedName

data class SmsDto(
    @SerializedName("from")
    val from: String,
    @SerializedName ("to")
    val to: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("bulkVariant")
    val bulkVariant: String,
    @SerializedName("doubleEncode")
    val doubleEncode: Boolean
)