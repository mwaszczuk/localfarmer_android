package pl.solventive.localfarmer.data.network.api

import io.reactivex.Single
import pl.solventive.localfarmer.data.network.dto.PostingDto
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadPostingDto
import retrofit2.http.*

interface PostingsApi {

    @GET("v1/postings")
    fun getPostings(
        @Header("Authorization") token: String
    ) : Single<List<PostingDto>>

    @GET("v1/postings/{id}")
    fun getPosting(
        @Header("Authorization") token: String,
        @Path("id") id: String
    ) : Single<PostingDto>

    @POST("v1/postings")
    fun postPosting(
        @Header("Authorization") token: String,
        @Body posting: UploadPostingDto
    ) : Single<PostingDto>

}