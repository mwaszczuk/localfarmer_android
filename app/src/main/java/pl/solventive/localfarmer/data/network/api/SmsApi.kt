package pl.solventive.localfarmer.data.network.api

import io.reactivex.Single
import pl.solventive.localfarmer.data.network.dto.SmsResponseDto
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.http.Url

interface SmsApi {

    @POST
    fun sendSms(
        @Url url: String,
        @Query("key") key: String,
        @Query("password") password: String,
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("msg") msg: String
    ) : Single<SmsResponseDto>
}