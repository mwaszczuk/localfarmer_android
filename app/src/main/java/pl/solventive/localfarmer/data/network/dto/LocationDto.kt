package pl.solventive.localfarmer.data.network.dto

data class LocationDto(
    val id: String,
    val name: String,
    val userId: Int,
    val city: String,
    val street: String?,
    val postalCode: String?,
    val buildingNumber: String?,
    val latitude: Double,
    val longitude: Double

)