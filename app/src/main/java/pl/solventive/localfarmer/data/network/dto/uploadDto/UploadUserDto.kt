package pl.solventive.localfarmer.data.network.dto.uploadDto

import org.joda.time.DateTime

data class UploadUserDto(
    val name: String? = null,
    val surname: String? = null,
    val farmName: String? = null,
    val phoneNumber: String? = null,
    val accountType: Int? = null,
    val description: String? = null,
    val ratings: Int? = null,
    val ratingPoints: Double? = null,
    val createdAt: DateTime? = null,
    val profilePhotoId: String? = null,
    val backgroundPhotoId: String? = null,
    val locationId: String? = null,
    val lastModifiedAt: DateTime? = null,
    val lastLoginAt: DateTime? = null,
    val lastActivityAt: DateTime? = null
)