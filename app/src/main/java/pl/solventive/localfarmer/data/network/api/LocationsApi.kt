package pl.solventive.localfarmer.data.network.api

import io.reactivex.Single
import pl.solventive.localfarmer.data.network.dto.LocationDto
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadLocationDto
import retrofit2.http.*

interface LocationsApi {

    @GET("v1/locations")
    fun getLocations(
        @Header("Authorization") token: String
    ) : Single<List<LocationDto>>

    @GET("v1/locations/{id}")
    fun getLocation(
        @Header("Authorization") token: String,
        @Path("id") id: String
    ) : Single<LocationDto>

    @POST("v1/locations")
    fun postLocation(
        @Header("Authorization") token: String,
        @Body posting: UploadLocationDto
    ) : Single<LocationDto>

}