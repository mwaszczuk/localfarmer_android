package pl.solventive.localfarmer.data.network.repository

import okhttp3.Credentials
import pl.solventive.localfarmer.BuildConfig
import pl.solventive.localfarmer.data.network.api.LoginApi
import javax.inject.Inject

class LoginRepository @Inject constructor(
    private val api: LoginApi
) {

    fun login(
        phoneNumber: String,
        password: String
    ) = api.loginUser(Credentials.basic(BuildConfig.SECRET, BuildConfig.SECRET_PW),
        phoneNumber,
        password,
        "password")
}