package pl.solventive.localfarmer.data.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update
import pl.solventive.localfarmer.data.database.entity.ProductCategory

@Dao
interface ProductCategoryDao {

    @Query("SELECT * FROM product_category WHERE id = :categoryId LIMIT 1")
    fun selectCategoryById(categoryId: Int): LiveData<ProductCategory>

    @Query("SELECT * FROM product_category")
    fun selectAllCategories(): LiveData<List<ProductCategory>>

    @Update
    fun update(category: ProductCategory)

    @Insert(onConflict = REPLACE)
    fun insert(category: ProductCategory)

    @Insert(onConflict = REPLACE)
    fun insert(categories: List<ProductCategory>)
}