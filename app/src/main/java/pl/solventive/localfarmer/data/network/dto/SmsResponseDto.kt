package pl.solventive.localfarmer.data.network.dto

import com.google.gson.annotations.SerializedName

data class SmsResponseDto(
    @SerializedName("messageId")
    val messageId: String?,
    @SerializedName("errorMsg")
    val errorMsg: String?,
    @SerializedName("errorCode")
    val errorCode: String?
)