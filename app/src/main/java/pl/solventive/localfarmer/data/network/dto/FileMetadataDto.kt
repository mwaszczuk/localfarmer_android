package pl.solventive.localfarmer.data.network.dto

import com.google.gson.annotations.SerializedName

data class FileMetadataDto(
    @SerializedName("id")
    val id: String,
    @SerializedName("fileType")
    val fileType: String,
    @SerializedName("fileName")
    val fileName: String
)