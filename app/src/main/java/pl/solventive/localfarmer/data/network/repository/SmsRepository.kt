package pl.solventive.localfarmer.data.network.repository

import pl.solventive.localfarmer.BuildConfig
import pl.solventive.localfarmer.data.network.api.SmsApi
import javax.inject.Inject

class SmsRepository @Inject constructor(
    private val api: SmsApi
) {

    fun sendSms(to: String, msg: String)
            = api.sendSms(
        BuildConfig.SMS_API_ADDRESS,
        BuildConfig.SMS_API_KEY,
        "VCk1U9eZ",
        "LocalFarmer",
        to,
        msg
    )

}