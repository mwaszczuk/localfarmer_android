package pl.solventive.localfarmer.data.network.api

import io.reactivex.Single
import pl.solventive.localfarmer.data.network.dto.OAuthToken
import retrofit2.http.*

interface LoginApi {

    @FormUrlEncoded
    @POST("oauth/token")
    fun loginUser(
        @Header("Authorization") basicAuth: String,
        @Field("username") phoneNumber: String,
        @Field("password") password: String,
        @Field("grant_type") grantType: String
    ) : Single<OAuthToken>
}