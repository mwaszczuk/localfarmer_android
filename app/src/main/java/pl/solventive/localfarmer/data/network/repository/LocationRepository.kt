package pl.solventive.localfarmer.data.network.repository

import android.content.SharedPreferences
import pl.solventive.localfarmer.data.network.api.LocationsApi
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadLocationDto
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadPostingDto
import pl.solventive.localfarmer.util.extension.getOAuthToken
import javax.inject.Inject

class LocationRepository @Inject constructor(
    private val prefs: SharedPreferences,
    private val api: LocationsApi
) {

    fun getLocations() = api.getLocations(prefs.getOAuthToken())

    fun getLocation(locationId: String) = api.getLocation(prefs.getOAuthToken(), locationId)

    fun addLocation(
        location: UploadLocationDto
    ) = api.postLocation(prefs.getOAuthToken(), location)
}