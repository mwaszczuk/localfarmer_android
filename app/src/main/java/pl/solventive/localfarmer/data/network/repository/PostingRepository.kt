package pl.solventive.localfarmer.data.network.repository

import android.content.SharedPreferences
import pl.solventive.localfarmer.data.network.api.PostingsApi
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadPostingDto
import pl.solventive.localfarmer.util.extension.getOAuthToken
import javax.inject.Inject

class PostingRepository @Inject constructor(
    private val prefs: SharedPreferences,
    private val api: PostingsApi
) {

    fun getPostings() = api.getPostings(prefs.getOAuthToken())

    fun getPosting(postingId: String) = api.getPosting(prefs.getOAuthToken(), postingId)

    fun addPosting(
        posting: UploadPostingDto
    ) = api.postPosting(prefs.getOAuthToken(), posting)
}