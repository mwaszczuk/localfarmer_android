package pl.solventive.localfarmer.data.database

import androidx.room.TypeConverter
import java.util.*

class RoomConverter {

    companion object {
        @JvmStatic
        @TypeConverter
        fun fromTimestamp(value: Long?): Date?
            = if (value == null) null else Date(value)

        @JvmStatic
        @TypeConverter
        fun dateToTimestamp(date: Date?): Long?
            = date?.time
    }

}