package pl.solventive.localfarmer.data.database.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import pl.solventive.localfarmer.data.database.dao.ProductUnitDao
import pl.solventive.localfarmer.data.database.entity.ProductUnit
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class ProductUnitRepository @Inject constructor(
    private val unitDao: ProductUnitDao
) : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    fun getProductUnits() = unitDao.selectAllUnits()
    fun setProductUnit(unit: ProductUnit) = launch { setProductUnitAsync(unit) }
    fun updateProductUnit(unit: ProductUnit) = launch { updateProductUnitAsync(unit) }
    fun setProductUnits(units: List<ProductUnit>) = launch { setProductUnitsAsync(units) }
    fun getUnitById(id: Int) = unitDao.selectUnitById(id)


    private suspend fun getUnitByIdAsync(id: Int) =
        withContext(Dispatchers.IO) {
            unitDao.selectUnitById(id)
        }

    private suspend fun setProductUnitAsync(unit: ProductUnit) =
        withContext(Dispatchers.IO) {
            unitDao.insert(unit)
        }

    private suspend fun updateProductUnitAsync(unit: ProductUnit) =
        withContext(Dispatchers.IO) {
            unitDao.update(unit)
        }

    private suspend fun setProductUnitsAsync(units: List<ProductUnit>) {
        withContext(Dispatchers.IO) {
            unitDao.insert(units)
        }
    }

}