package pl.solventive.localfarmer.data.network.api

import io.reactivex.Single
import pl.solventive.localfarmer.data.network.dto.UserDto
import pl.solventive.localfarmer.data.network.dto.uploadDto.RegisterUserDto
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadUserDto
import retrofit2.http.*

interface UsersApi {

    @GET("v1/users/verify/{userPhoneNumber}")
    fun verifyPhoneNumber(
        @Header("Authorization") token: String,
        @Path("userPhoneNumber") phoneNumber: String
    ) : Single<Boolean>

    @GET("v1/users")
    fun getUsers(
        @Header("Authorization") token: String
    ) : Single<List<UserDto>>

    @GET("v1/users/{userId}")
    fun getUser(
        @Header("Authorization") token: String,
        @Path("userId") userId: Int
    ) : Single<UserDto>

    @POST("v1/users/register")
    fun createUser(
        @Header("Authorization") token: String,
        @Body user: RegisterUserDto
    ) : Single<UserDto>

    @PUT("v1/users/{userId}")
    fun putUser(
        @Header("Authorization") token: String,
        @Path("userId") userId: Int,
        @Body user: UploadUserDto
    ) : Single<UserDto>
}