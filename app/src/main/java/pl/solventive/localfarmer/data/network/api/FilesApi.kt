package pl.solventive.localfarmer.data.network.api

import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import pl.solventive.localfarmer.data.network.dto.FileMetadataDto
import pl.solventive.localfarmer.data.network.dto.MultipleFilesMetadataDto
import retrofit2.http.*

interface FilesApi {

    @GET("v1/files/downloadFile/{id}")
    fun downloadFile(
        @Header("Authorization") token: String,
        @Path("id") fileId: String
    ) : Single<ResponseBody>

    @Multipart
    @POST("v1/files/uploadFile")
    fun postFile(
        @Header("Authorization") token: String,
        @Part file: MultipartBody.Part
    ) : Single<FileMetadataDto>

    @Multipart
    @POST("v1/files/uploadMultipleFiles")
    fun postMultipleFiles(
        @Header("Authorization") token: String,
        @Part files: List<MultipartBody.Part>
    ) : Single<List<MultipleFilesMetadataDto>>

}