package pl.solventive.localfarmer.data.network.repository

import android.content.SharedPreferences
import pl.solventive.localfarmer.data.network.api.ProductsApi
import pl.solventive.localfarmer.util.extension.getOAuthToken
import javax.inject.Inject

class ProductRepository @Inject constructor(
    private val prefs: SharedPreferences,
    private val api: ProductsApi
) {

    fun getProductCategories() = api.getProductCategories(prefs.getOAuthToken())

    fun getProductUnits() = api.getProductUnits(prefs.getOAuthToken())

}