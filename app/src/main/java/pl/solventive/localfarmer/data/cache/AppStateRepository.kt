package pl.solventive.localfarmer.data.cache

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.auth0.android.jwt.JWT
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import pl.solventive.localfarmer.BuildConfig
import pl.solventive.localfarmer.data.network.dto.OAuthToken
import pl.solventive.localfarmer.util.extension.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppStateRepository @Inject constructor(
    private val prefs: SharedPreferences
) {

    private val isOnline = MediatorLiveData<Boolean>()
    fun setOnline(isOnline: Boolean) { this.isOnline.postValue(isOnline) }
    fun isOnline(): LiveData<Boolean> = isOnline

    init {
        isOnline.postValue(true)
    }

    fun saveUserPhone(phone: String) { prefs.saveUserPassword(phone) }
    fun getUserPhone() = prefs.getUserPhone()

    fun saveUserPassword(password: String) { prefs.saveUserPassword(password) }
    fun getUserPassword() = prefs.getUserPassword()

    fun saveUserName(name: String) { prefs.saveUserName(name) }
    fun getUserName() = prefs.getUserName()

    fun getUserId() = prefs.getUserId()

    fun saveRefreshToken(token: String) { prefs.saveRefreshToken(token) }
    fun getRefreshToken() = prefs.getRefreshToken()

    fun saveOAuthToken(token: String) { prefs.saveOAuthToken("Bearer ".plus(token)) }
    fun getOAuthToken() = prefs.getOAuthToken()

//    fun clearData() = database.clearAllTables()
    fun clearUserData() {
//        database.clearAllTables()
        prefs.clearUserData()
    }

    fun saveUserData(token: OAuthToken, phoneNumber: String, password: String) {
        val parsedJWT = JWT(token.token)
        val userId = parsedJWT.getClaim("userId").asInt() ?: -1
        val bearerToken = token.token
        prefs.saveOAuthToken("Bearer ".plus(bearerToken))
        prefs.saveRefreshToken(token.refreshToken)
        if (userId > 1) {
            prefs.saveUserId(userId)
            prefs.saveUserPhone(phoneNumber)
            prefs.saveUserPassword(password)
        }
    }

    fun createGlideUrl(photoId: String) = GlideUrl(
        BuildConfig.API_ADDRESS.plus("v1/files/downloadFile/").plus(photoId),
        LazyHeaders.Builder()
            .addHeader("Authorization", getOAuthToken())
            .build()
    )
}