package pl.solventive.localfarmer.data.network.dto.uploadDto

import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime

data class RegisterUserDto(
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("accountType")
    val accountType: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("createdAt")
    val createdAt: DateTime
)