package pl.solventive.localfarmer.data.network.dto

import com.google.gson.annotations.SerializedName

data class MultipleFilesMetadataDto(
    @SerializedName("statusCode")
    val statusCode: String,
    @SerializedName("statusCodeValue")
    val statusCodeValue: Int,
    @SerializedName("body")
    val fileMetadata: FileMetadataDto
)