package pl.solventive.localfarmer.data.network.repository

import android.content.SharedPreferences
import pl.solventive.localfarmer.data.network.api.UsersApi
import pl.solventive.localfarmer.data.network.dto.uploadDto.RegisterUserDto
import pl.solventive.localfarmer.data.network.dto.uploadDto.UploadUserDto
import pl.solventive.localfarmer.util.extension.getOAuthToken
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val prefs: SharedPreferences,
    private val api: UsersApi
) {

    fun verifyPhoneNumber(
        phoneNumber: String
    ) = api.verifyPhoneNumber(prefs.getOAuthToken(), phoneNumber)

    fun getUsers() = api.getUsers(prefs.getOAuthToken())

    fun getUser(userId: Int) = api.getUser(prefs.getOAuthToken(), userId)

    fun createUser(
        user: RegisterUserDto
    ) = api.createUser(prefs.getOAuthToken(), user)

    fun putUser(
        userId: Int,
        user: UploadUserDto
    ) = api.putUser(prefs.getOAuthToken(), userId, user)
}