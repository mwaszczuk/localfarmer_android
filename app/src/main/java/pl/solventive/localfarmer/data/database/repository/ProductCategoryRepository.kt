package pl.solventive.localfarmer.data.database.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import pl.solventive.localfarmer.data.database.dao.ProductCategoryDao
import pl.solventive.localfarmer.data.database.entity.ProductCategory
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class ProductCategoryRepository @Inject constructor(
    private val categoryDao: ProductCategoryDao
) : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    fun getProductCategories() = categoryDao.selectAllCategories()
    fun setProductCategory(category: ProductCategory) = launch { setProductCategoryAsync(category) }
    fun updateProductCategory(category: ProductCategory) = launch { updateProductCategoryAsync(category) }
    fun setProductCategories(categories: List<ProductCategory>) = launch { setProductCategoriesAsync(categories) }
    fun getCategoryById(id: Int) = launch { getCategoryByIdAsync(id) }

    private suspend fun getCategoryByIdAsync(id: Int) =
        withContext(Dispatchers.IO) {
            categoryDao.selectCategoryById(id)
        }

    private suspend fun setProductCategoryAsync(category: ProductCategory) =
        withContext(Dispatchers.IO) {
            categoryDao.insert(category)
        }

    private suspend fun updateProductCategoryAsync(category: ProductCategory) =
        withContext(Dispatchers.IO) {
            categoryDao.update(category)
        }

    private suspend fun setProductCategoriesAsync(categories: List<ProductCategory>) =
        withContext(Dispatchers.IO) {
            categoryDao.insert(categories)
        }

}