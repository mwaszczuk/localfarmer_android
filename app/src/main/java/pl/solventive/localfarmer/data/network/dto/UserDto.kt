package pl.solventive.localfarmer.data.network.dto

import org.joda.time.DateTime

data class UserDto(
    val id: Int,
    val name: String,
    val surname: String?,
    val farmName: String?,
    val phoneNumber: String,
    val accountType: Int,
    val description: String?,
    val ratings: Int,
    val ratingPoints: Double,
    val createdAt: DateTime,
    val profilePhotoId: String?,
    val backgroundPhotoId: String?,
    val locationId: String?,
    val lastModifiedAt: DateTime?,
    val lastLoginAt: DateTime?,
    val lastActivityAt: DateTime?
)