package pl.solventive.localfarmer.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import pl.solventive.localfarmer.data.database.dao.ProductCategoryDao
import pl.solventive.localfarmer.data.database.dao.ProductUnitDao
import pl.solventive.localfarmer.data.database.entity.ProductCategory
import pl.solventive.localfarmer.data.database.entity.ProductUnit

@Database(entities = [
    ProductUnit::class,
    ProductCategory::class
], version = 1)
@TypeConverters(RoomConverter::class)
abstract class Database : RoomDatabase() {

    abstract fun productUnitDao(): ProductUnitDao
    abstract fun productCategoryDao(): ProductCategoryDao

}