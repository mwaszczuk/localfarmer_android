package pl.solventive.localfarmer.data

import okhttp3.Interceptor
import okhttp3.Response

class OkHttpInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
                .newBuilder()
                .addHeader("SOT-ApplicationName", "mobilemanager")
                .build()

        return chain.proceed(request)
    }

}