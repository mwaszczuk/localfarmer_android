package pl.solventive.localfarmer.data.network.dto.uploadDto

import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime

data class UploadPostingDto(
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("userId")
    val userId: Int,
    @SerializedName("quantityUnitId")
    val quantityUnitId: Int,
    @SerializedName("categoryId")
    val categoryId: Int,
    @SerializedName("quantity")
    val quantity: Double,
    @SerializedName("price")
    val price: Double,
    @SerializedName("tags")
    val tags: List<String>? = null,
    @SerializedName("mainPhotoId")
    val mainPhotoId: String,
    @SerializedName("photos")
    val photos: List<String>,
    @SerializedName("latitude")
    val latitude: Double? = null,
    @SerializedName("longitude")
    val longitude: Double? = null,
    @SerializedName("createdAt")
    val createdAt: DateTime,
    @SerializedName("expiryDate")
    val expiryDate: DateTime,
    @SerializedName("status")
    val status: Int
)