package pl.solventive.localfarmer

import androidx.multidex.MultiDexApplication
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import pl.solventive.localfarmer.util.injection.AppInjector
import timber.log.Timber
import javax.inject.Inject

class LocalFarmerApplication : MultiDexApplication(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    companion object {
        var API_ADRESS = BuildConfig.API_ADDRESS
    }

    override fun onCreate() {
        super.onCreate()

        if (android.os.Build.VERSION.SDK_INT < 23) {
            setTheme(R.style.AppThemeBelow23)
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        AppInjector.init(this)
    }
}