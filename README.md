# LocalFarmer Android app

An app for agricultural transactions (posting service) based on self-made API. 


### Installing

You can either: 

1. Clone the repository and build the app with Android studio.

2. [Download the APK file directly from here.](http://46.41.148.210/download/apk/localfarmer)


## Libraries / Frameworks used in this project

* [Dagger2](https://github.com/google/dagger) - for dependency injection
* [Retrofit](https://github.com/square/retrofit) - for API requests
* [Glide](https://github.com/bumptech/glide) - for download and loading images
* [Cropper](https://github.com/edmodo/cropper/wiki) - android image cropper
* [Compressor](https://github.com/zetbaitsu/Compressor) - image compressor
* [JWTDecode](https://github.com/auth0/JWTDecode.Android) - for decoding jwt
* [osmdroid](https://github.com/osmdroid/osmdroid) - for handling OpenStreetMap
* [ktopencage](https://github.com/sgimp/ktopencage) - for handling Open Cage Geocoder


## Authors

* **Marek Waszczuk** -   [mwaszczuk](https://gitlab.com/mwaszczuk)


